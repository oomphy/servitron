import com.oomph.clients._
import com.oomph.configuration.{ApiConfiguration, Request}
import java.util
import java.util.zip.ZipEntry
import org.apache.pdfbox.cos.{COSStream, COSName, COSBase}
import org.apache.pdfbox.pdmodel.common.PDRectangle
import org.apache.pdfbox.pdmodel.graphics.PDGraphicsState
import org.apache.pdfbox.pdmodel.graphics.xobject.{PDXObjectForm, PDXObjectImage}
import org.apache.pdfbox.pdmodel.{PDPage, PDDocument}
import org.apache.pdfbox.util.{PDFOperator, PDFStreamEngine}
import dispatch.Http
import scalaz.syntax.foldable._
import scalaz.std.list._

case class FilePath(v: String) {
  def extension = v.lastIndexOf(".") match {
    case -1 => None
    case x => Some(v.substring(x, v.length).toLowerCase)
  }
}

case class GB(v: Float)

object GB {
  def fromBytes(v: Long) = GB(v.toFloat / 1024 / 1024)

  implicit val ordering: Ordering[GB] = Ordering.by(_.v)
}

object C {
  System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
  def extStatsForZip(path: String): Map[String, GB] = {
    import scala.collection.JavaConversions._
    import scalaz.std.map.insertWith
    println(path)
    val zip = new java.util.zip.ZipFile(path)
    val entries = zip.entries
    entries
      .filter(!_.isDirectory)
      .flatMap(f => FilePath(f.getName).extension.map(_ -> f.getSize))
      .foldLeft(Map.empty[String, Long])((m, stuff) => { insertWith(m, stuff._1, stuff._2)(_ + _)})
      .mapValues(GB.fromBytes)
  }
}

object ContentMap {
  def main(args:Array[String]) {
    val conf = ApiConfiguration.create("https://api.oomphhq.com/publish/api/v1", "what", "foo")
    val dashboard = Dashboard(Request.fromConfiguration(conf))
    val issues2013 = loadIssues(dashboard, "au.com.acpmagazines.gourmettraveller", (_.itunes_product_id contains "2013"))
    issues2013 foreach { case (issue, Some(ivc)) =>
      
    }
  }

  def loadIssues(dashboard: Dashboard, app: String, filter: (Issue => Boolean)): List[(Issue, Option[IssueVersionCompact])] = {
    val http = new Http
    val issues = http(dashboard.issuesForApplication(app)).filter(filter)
    val x =issues map { issue =>
      (issue, (http(dashboard.issueVersionsForIssue(issue.itunes_product_id)):
        List[IssueVersionCompact]).maximum(implicitly[scalaz.Order[java.util.Date]].contramap(_.updatedAt)))
    }
    http.shutdown()
    x
  }

  def main1(args: Array[String]) {
    val stats = C.extStatsForZip("content-map/november-december-2012.zip")
    stats.toList.sortBy(_._2).foreach { f =>
      val ext = f._1
      val countMb = f._2.v
      println("%s: %f" format(ext.padTo(8, " ").mkString, countMb))
    }
    println("=============")
    println("          %f" format (stats.toList.map(_._2.v).sum))
    println("=============")
  }
}
