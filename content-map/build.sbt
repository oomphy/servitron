name := "content-map"

version := "1.0"

scalaVersion := "2.9.2"

libraryDependencies ++= Seq(
  "org.apache.pdfbox" % "pdfbox" % "1.6.0" withSources(),
  // Dropwizard
  "com.yammer.dropwizard" % "dropwizard-core" % "0.5.1",
  "com.yammer.dropwizard" % "dropwizard-client" % "0.5.1",
  "com.yammer.dropwizard" % "dropwizard-db" % "0.5.1",
  "com.yammer.dropwizard" % "dropwizard-scala_2.9.1" % "0.5.1",
  "javax.servlet" % "javax.servlet-api" % "3.0.1",
  "org.scalaz" %% "scalaz-core" % "7.0-SNAPSHOT",
  "org.scalaquery" % "scalaquery_2.9.1-1" % "0.10.0-M1",
  "mysql" % "mysql-connector-java" % "5.1.21",
  "joda-time" % "joda-time" % "2.1",
  "org.joda" % "joda-convert" % "1.2"
)

libraryDependencies ++= Seq(
  "org.scalacheck" %% "scalacheck" % "1.10.0" % "test",
  "org.specs2" %% "specs2" % "1.12" % "test",
  "com.h2database" % "h2" % "1.3.168" % "test"
)

//seq(com.github.retronym.SbtOneJar.oneJarSettings: _*)

resolvers += "Codahale's Repo" at "http://repo.codahale.com" // Dropwizard

resolvers += "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/" // Argonaut and scalaz
