import sbt._

object PluginBuild extends Build {
  override lazy val projects = Seq(root)
  lazy val root = Project("plugins", file(".")).dependsOn(stage)
  lazy val stage = uri("git://github.com/nkpart/sbt-stage-plugin")
}
