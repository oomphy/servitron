import sbt._
import Keys._

object ServitronBuild extends Build {
  lazy val servitron = Project(id = "servitron", base = file(".")) aggregate(cdnStats, indexer, common, contentMap)

  lazy val cdnStats = Project(id = "cdn-stats", base = file("cdn-stats")) dependsOn(common)

  lazy val contentMap = Project(id = "content-map", base = file("content-map")) dependsOn(common)

  lazy val indexer = Project(id = "indexer", base = file("indexer")) dependsOn(common)

  lazy val common = Project(id = "common", base = file("common"))
}
