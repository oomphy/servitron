# scala common

## Overview

Common scala code.

## Setup

    $ brew install sbt
    $ sbt
    > update
    > run

## Deployment

You will need to push this into your local ivy repo before you can reference it in other projects:

    $ sbt
    > publish-local

See: https://github.com/harrah/xsbt/wiki/Publishing

Note. The version number you select must end with SNAPSHOT, or you must change the version number each time you publish.
Ivy maintains a cache, and it stores even local projects in that cache. If Ivy already has a version cached, it will not
check the local repository for updates, unless the version number matches a changing pattern, and SNAPSHOT is one such pattern.

## Using

To use this in a project, add this to your `build.sbt`:

    libraryDependencies += "com.oomphhq" %% "scala-commons" % "1.0.0-SNAPSHOT" withSources()

## IDEA

If you make changes to dependencies run this in an sbt session:

    > gen-idea no-fsc

And accept the IDEA prompts to reload. Note that this doesn't use FSC.

## See Also

https://github.com/sbt/sbt-release
