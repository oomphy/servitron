package com.oomphhq.common.util.test

import java.io._

import com.oomphhq.common.util.io.Directory._
import com.oomphhq.common.util.io.File._
import com.oomphhq.common.util.io.{Directory, File}

trait TestResource {self =>
  def testResourceStream(relativePath: String): InputStream = self.getClass.getResource(relativePath).openStream()

  def testResourceFile(relativePath: String): File = file(absolute(relativePath))

  def testResourceDirectory(relativePath: String): Directory = directory(absolute(relativePath))

  def packageRoot = {
    val fqn = self.getClass.getName
    val classResourceNoLeadingSlash = fqn.replace('.', '/') + ".class"
    val classResourceUrl = self.getClass.getResource('/' + classResourceNoLeadingSlash)
    val absolutePath = classResourceUrl.getFile
    directory(absolutePath.replace(classResourceNoLeadingSlash, ""))
  }

  private def absolute(relativePath: String) = packageRoot.path + relativePath
}
