package com.oomphhq.common.util.io

import java.io.{File => JavaFile}

trait FilesystemItem {
  def exists: Boolean

  def name: String

  def path: String

  def file: JavaFile

  def parent: Option[Directory]
}
