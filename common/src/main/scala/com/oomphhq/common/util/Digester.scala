package com.oomphhq.common.util

import java.security.MessageDigest
import java.io.InputStream

object Digester {
  def md5(in: InputStream): String = {
    val digest = MessageDigest.getInstance("MD5");
    val buffer = new Array[Byte](2048)
    def read() {
      in.read(buffer) match {
        case bytesRead if bytesRead < 0 =>
        case 0 => read()
        case bytesRead => digest.update(buffer, 0, bytesRead); read()
      }
    }
    read()
    bytes2Hex(digest.digest())
  }

  private def bytes2Hex(bytes: Array[Byte]): String = {
    def cvtByte(b: Byte): String = {
      (if ((b & 0xff) < 0x10) "0" else "") + java.lang.Long.toString(b & 0xff, 16)
    }
    bytes.map(cvtByte(_)).mkString
  }
}
