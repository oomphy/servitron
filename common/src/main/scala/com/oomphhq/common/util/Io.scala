package com.oomphhq.common.util

import io._, Directory._, File._
import scala.sys
import java.io.{OutputStream, ByteArrayOutputStream, FileOutputStream, InputStream, File => JavaFile}
import org.apache.commons.io.FileUtils

object Io {
  def withTempDir[A](d: Directory => A): A = {
    val tmpDir = createTempDirectory
    try {
      d(directory(tmpDir))
    } finally {
      FileUtils.deleteDirectory(tmpDir)
    }
  }

  def withTempFile[A](f: File => A): A = {
    val tmp = JavaFile.createTempFile("temp", null)
    try {
      f(file(tmp))
    } finally {
      tmp.delete()
    }
  }

  def toFile(in: InputStream): File = {
    val f = JavaFile.createTempFile("input", ".pdf")
    val out = new FileOutputStream(f)
    try {
      copy(in, out)
    } finally {
      close(out)
    }
    file(f)
  }

  def toBytes(in: InputStream): Array[Byte] = {
    val out = new ByteArrayOutputStream
    copy(in, out)
    out.toByteArray
  }

  def slurp(in: InputStream): String = {
    val out = new ByteArrayOutputStream
    copy(in, out)
    out.toString("UTF-8")
  }

  def copy(src: File, dest: File) {
    dest.out() { out => src.in { in => copy(in, out)}}
  }

  def copy(in: InputStream, out: OutputStream) {
    val buffer = new Array[Byte](2048)
    def read() {
      in.read(buffer) match {
        case bytesRead if bytesRead < 0 =>
        case 0 => read()
        case bytesRead => out.write(buffer, 0, bytesRead); read()
      }
    }
    read()
  }

  def close(closable: {def close()}) {
    try {
      closable.close()
    } catch {
      case e =>
    }
  }

  // Taken from: http://code.google.com/p/guava-libraries/source/browse/guava/src/com/google/common/io/Files.java
  def createTempDirectory: JavaFile = {
    val creationAttempts = 10000
    val baseDir = new JavaFile(sys.props("java.io.tmpdir"))
    val baseName = System.currentTimeMillis() + "-"
    for (i <- 0 until creationAttempts) {
      val tempDir = new JavaFile(baseDir, baseName + i)
      if (tempDir.mkdir()) {
        return tempDir
      }
    }
    sys.error("Failed to create directory (%s) within %s attempts".format(baseName, creationAttempts))
  }
}
