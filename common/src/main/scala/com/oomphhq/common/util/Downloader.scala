package com.oomphhq.common.util

import dispatch._, Request._
import io.File

object Downloader {
  def downloadToFile(url: String, destination: File) {
    val http = new Http
    destination.out() {s =>
      try {
        http(url >>> s)
      } finally {
        http.shutdown()
      }
    }
  }
}
