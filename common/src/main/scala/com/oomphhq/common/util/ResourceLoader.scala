package com.oomphhq.common.util

import java.net.URL
import java.io.InputStream

object ResourceLoader {
  def resourceUrl(path: String): Option[URL] = Option(getResourceAsUrl(canonicalise(path)))

  def resourceStream(path: String): Option[InputStream] = Option(getResourceAsStream(canonicalise(path)))

  def getResourceAsUrl(path: String): URL = ResourceLoader.getClass.getResource(path)

  def getResourceAsStream(path: String): InputStream = ResourceLoader.getClass.getResourceAsStream(path)

  def canonicalise(path: String): String = if (path.startsWith("/")) path else "/" + path
}
