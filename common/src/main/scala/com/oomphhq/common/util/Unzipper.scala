package com.oomphhq.common.util

import io.{Directory, File}, Directory._, File._
import scala.collection.JavaConverters._
import org.apache.commons.compress.archivers.zip.{ZipArchiveEntry, ZipFile}

object Unzipper {
  val ignoredFiles = List("__MACOSX", ".DS_Store")

  def unzip(srcZip: File, extractBaseDir: Directory) {
    val zipFile = new ZipFile(srcZip.file)
    try {
      zipFile.getEntries.asScala.foreach {
        extractEntry(extractBaseDir, zipFile, _)
      }
    } finally {
      Io.close(zipFile)
    }
  }

  private def extractEntry(baseDir: Directory, zipFile: ZipFile, entry: ZipArchiveEntry) {
    val canBeExtracted = ignoredFiles.lastIndexWhere(f => entry.getName.contains(f)) == -1
    if (canBeExtracted) {
      if (entry.isDirectory) {
        directory(baseDir, entry.getName).mkdirs()
      } else {
        // Note. Need to create the directory the file is in if the zip does not have directories in it.
        val root = file(baseDir, entry.getName).file.getPath.split("/").dropRight(1).mkString("/")
        directory(root).mkdirs()
        file(baseDir, entry.getName).out() { out =>
          val in = zipFile.getInputStream(entry)
          try {
            Io.copy(in, out)
          } finally {
            Io.close(in)
          }
        }
      }
    }
  }
}
