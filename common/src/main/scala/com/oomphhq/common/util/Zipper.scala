package com.oomphhq.common.util

import io.{File, FilesystemItem, Directory}
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream
import java.io.OutputStream

trait ZipAdd {
  def f: FilesystemItem
}

case class ZipAddFile(f: File, overridePath: Option[String]) extends ZipAdd
case class ZipAddDir(f: Directory) extends ZipAdd

object Zipper {
  def zip(destination: OutputStream, src: ZipAdd*) {
    val out = new ZipArchiveOutputStream(destination)
    try {
      src.foreach(s => {
        val root = "%s/".format(s.f.file.getParent)
        s match {
          case ZipAddFile(f, overridePath) => addFileToZip(root, f, overridePath, out)
          case ZipAddDir(d: Directory) => addDirectoryToZip(root, d, out)
        }
      })
    } finally {
      Io.close(out)
    }
  }

  private def addDirectoryToZip(root: String, src: Directory, out: ZipArchiveOutputStream) {
    src.listFiles().foreach(addFileToZip(root, _, None, out))
    src.listDirectories().foreach(d => addDirectoryToZip(root, d, out))
  }

  private def addFileToZip(root: String, src: File, overridePath: Option[String], out: ZipArchiveOutputStream) {
    src.in(in => {
      try {
        out.putArchiveEntry(out.createArchiveEntry(src.file, overridePath.getOrElse(src.path.replaceAll(root, ""))))
        Io.copy(in, out)
        out.closeArchiveEntry()
      } finally {
        Io.close(in)
      }
    })
  }
}
