package com.oomphhq.common.util

object Timer {
  def time(f: => Unit): Long = {
    val start = System.currentTimeMillis()
    f
    System.currentTimeMillis() - start
  }
}
