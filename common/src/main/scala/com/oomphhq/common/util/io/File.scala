package com.oomphhq.common.util.io

import com.oomphhq.common.util.Io
import java.io.{PrintWriter, File => JavaFile, FileOutputStream, OutputStream, FileInputStream, InputStream}
import Directory._
import scalaz.std.option._
import java.util.regex.Pattern

sealed trait File extends FilesystemItem {
  lazy val extensionPattern = Pattern.compile(".*/?.*?([^/?*:;{}\\\\]+)\\.([^/?*:;{}\\\\]+)")

  val jFile: JavaFile

  def exists: Boolean = jFile.exists()

  def name: String = jFile.getName

  def extension: Option[String] = {
    val m = extensionPattern.matcher(name)
    if (m.matches()) some(m.group(2)) else none
  }

  def path: String = jFile.getCanonicalPath

  def file: JavaFile = jFile

  def parent: Option[Directory] = Option(jFile.getParentFile).map(directory(_))

  def move(destination: File): File = {
    jFile.renameTo(destination.file)
    destination
  }

  def in[A](f: InputStream => A): A = {
    val in = new FileInputStream(jFile)
    try {
      f(in)
    } finally {
      Io.close(in)
    }
  }

  def out(append: Boolean = false)(f: OutputStream => Unit) {
    val out = new FileOutputStream(jFile, append)
    try {
      f(out)
    } finally {
      Io.close(out)
    }
  }

  def writer(append: Boolean = false)(f: PrintWriter => Unit) {
    this.out(append) { s =>
      val w = new PrintWriter(s)
      try {
        f(w)
      } finally {
        Io.close(w)
      }
    }
  }
}

private case class File_(jFile: JavaFile) extends File

object File {
  def file(parent: Directory, name: String): File = file(new JavaFile(parent.file, name))

  def file(path: String): File =
    if (path == null) {
      sys.error("%s is not a file".format(path))
    } else {
      file(new JavaFile(path))
    }

  def file(f: JavaFile): File =
    if (f != null && (f.isFile || !f.exists())) {
      new File_(f)
    } else {
      sys.error("%s is not a file".format(f))
    }
}
