package com.oomphhq.common.util.io

import java.io.{FileFilter, File => JavaFile}
import java.io
import com.oomphhq.common.util.io.Directory._

sealed trait Directory extends FilesystemItem {
  val dir: JavaFile

  val allF: (File) => Boolean = { f => true}
  val allD: (Directory) => Boolean = { d => true}

  def exists: Boolean = dir.exists()

  def name: String = dir.getName

  def path: String = dir.getCanonicalPath

  def file: JavaFile = dir

  def parent: Option[Directory] = Option(dir.getParentFile).map(directory(_))

  def mkdirs() {
    dir.mkdirs()
  }

  def listDirectories(f: Directory => Boolean = allD): List[Directory] = {
    val dirs = dir.listFiles(new FileFilter {
      def accept(pathname: io.File) = { pathname != null && pathname.isDirectory && f(Directory.directory(pathname)) }
    })
    Option(dirs).map(ds => {ds.map(Directory.directory(_)).toList}).getOrElse(Nil)
  }

  def listFiles(f: File => Boolean = allF): List[File] = {
    val files = dir.listFiles(new FileFilter {
      def accept(pathname: io.File) = { pathname != null && pathname.isFile && f(File.file(pathname)) }
    })
    Option(files).map(fs => {fs.map(File.file(_)).toList}).getOrElse(Nil)
  }
}

private case class Directory_(dir: JavaFile) extends Directory

object Directory {
  def directory(parent: Directory, name: String): Directory =
    if (parent == null || name == null) {
      sys.error("%s/%s is not a directory".format(parent, name))
    } else {
      directory(new JavaFile(parent.file, name))
    }

  def directory(path: String): Directory =
    if (path == null) {
      sys.error("%s is not a directory".format(path))
    } else {
      directory(new JavaFile(path))
    }

  def directory(dir: JavaFile): Directory = {
    if (dir == null || dir.isFile) {
      sys.error("%s is not a directory".format(dir))
    } else {
      new Directory_(dir)
    }
  }
}
