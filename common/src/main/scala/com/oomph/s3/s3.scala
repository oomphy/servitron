package com.oomph.s3

import com.yammer.dropwizard.config.Configuration
import org.hibernate.validator.constraints.NotEmpty
import org.codehaus.jackson.annotate.JsonProperty
import com.amazonaws.auth.BasicAWSCredentials
import com.oomphhq.common.util.io.File
import scalaz._
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model._
import java.io.{OutputStream, InputStream}
import com.oomphhq.common.util.Io

case class S3Key(s: String)

class S3Configuration extends Configuration {
  @NotEmpty @JsonProperty
  val accessKey: String = null

  @NotEmpty @JsonProperty
  val secretKey: String = null

  @NotEmpty @JsonProperty
  val bucket: String = null

  override def toString: String = "<S3Configuration: %s>" format (bucket)
}

sealed trait MD5

trait FileStore[K] {
  def upload(f: File, key: K): String @@ MD5
  def download(key: K, f: File)

  def analyzeRemoteFile[A](key: K)(f: File => A): A =  {
    Io.withTempFile { localCopy =>
      download(key, localCopy)
      f(localCopy)
    }
  }
}

case class S3Client(config: S3Configuration) extends FileStore[S3Key] {
  val awsCredentials = new BasicAWSCredentials(config.accessKey, config.secretKey)

  def upload(f: File, key: S3Key): String @@ MD5 = {
    val s3 = new AmazonS3Client(awsCredentials)
    val r: PutObjectRequest = new PutObjectRequest(config.bucket, key.s, f.file).withMetadata(new ObjectMetadata())
    r.setCannedAcl(CannedAccessControlList.PublicRead)
    val x = s3.putObject(r)
    Tag[String, MD5](resultMd5(x))
  }

  def download(key: S3Key, f: File) {
    val s3 = new AmazonS3Client(awsCredentials)
    val obj: S3Object = s3.getObject(config.bucket, key.s)
    f.out()(copyStream(obj.getObjectContent, _))
  }

  private def resultMd5(r: PutObjectResult): String = {
    r.getETag.replaceAll("\"", "")
  }

  private def copyStream(input: InputStream, output: OutputStream) {
    val buffer = new Array[Byte](8196)
    var bytesRead: Int = input.read(buffer)
    while (bytesRead != -1) {
      output.write(buffer, 0, bytesRead)
      bytesRead = input.read(buffer)
    }
  }
}
