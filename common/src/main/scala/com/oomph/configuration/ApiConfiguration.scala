package com.oomph.configuration

import org.hibernate.validator.constraints.NotEmpty
import org.codehaus.jackson.annotate.{JsonIgnoreProperties, JsonProperty}

import scalaz._
import scalaz.Scalaz._

class ApiConfiguration {
  @NotEmpty @JsonProperty
  val uriBase: String = null

  @JsonProperty
  val user: String = null

  @JsonProperty
  val password: String = null

  def isAuthenticated: Boolean = user != null && password != null

  override def toString: String = "<ApiConfiguration: %s %s:%s>" format (uriBase, user, "*".multiply(password.length))
}

object ApiConfiguration {
  def create(_uriBase: String, _user: String, _password: String): ApiConfiguration = new ApiConfiguration {
    override val uriBase = _uriBase
    override val user = _user
    override val password = _password
  }
}

object Request {
  import dispatch.{url, Request}
  def fromConfiguration(config: ApiConfiguration): Request = {
    url(config.uriBase).as_!(config.user, config.password)
  }
}
