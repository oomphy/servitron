package com.oomph.dropwizard

import com.yammer.dropwizard.config.Environment
import dispatch.Http
import com.yammer.dropwizard.client.{HttpClientConfiguration, HttpClientFactory}
import com.yammer.dropwizard.lifecycle.Managed

object EnvironmentUtils {
  def managedHttp(environment: Environment, config: HttpClientConfiguration): Http = {
    val httpFactory = new HttpClientFactory(config)
    val http = new Http {override def make_client = httpFactory.build()}
    environment.manage(new Managed {
      def start() {}
      def stop() { http.shutdown() }
    })
    http
  }
}
