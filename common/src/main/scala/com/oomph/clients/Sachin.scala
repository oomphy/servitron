package com.oomph.clients

import dispatch.Request
import argonaut._, Argonaut._
import com.yammer.dropwizard.logging.Log
import com.oomph.s3.S3Key

final case class AvailableBundle(itunesProductId: String, contentKey: S3Key)

case class Jerb(startUrl: String, unstartUrl: String, successUrl: String, failureUrl: String)

case class BundleJerb(jerb: Jerb, bundle: AvailableBundle)

object Jsons {
  implicit lazy val decodeBundle: DecodeJson[BundleJerb] =
    DecodeJson.jdecode2L(BundleJerb.apply _)("actions", "payload")
  implicit lazy val decodeJerb: DecodeJson[Jerb] =
    DecodeJson.jdecode4L(Jerb.apply _)("start_url", "unstart_url", "success_url", "failed_url")
  implicit lazy val decodeAvailable: DecodeJson[AvailableBundle] =
    DecodeJson.jdecode2L((itp: String, key: String) => AvailableBundle(itp, S3Key(key)))("itunes_product_id", "s3_key")
}

case class Sachin(base: Request) {
  import Jsons._
  val log = Log.forThisClass()
  def indexingQueue = {
    (base / "index_requests").as_str ~> { j =>
      log.info("%s" format j)
      j.parseOption.map(_.arrayOrEmpty.flatMap(_.jdecode[BundleJerb].value)).getOrElse(sys.error("Expected JSON, got something else while parsing index requests"))
    }
  }
}