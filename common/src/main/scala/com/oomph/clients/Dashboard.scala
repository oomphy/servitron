package com.oomph.clients

import org.codehaus.jackson.map.util.{ISO8601Utils => Iso8601}
import java.util.Date

import argonaut._, Argonaut._
import dispatch.{Handler, Request}
import com.oomph.s3.S3Key
import java.net.URLEncoder

case class IssueVersion(
                         s3_key: String, // Because of JDBI, leave this as string. Change to newtype when we're using scalaquery/slick
                         itunes_product_id: String,
                         application_bundle_id: String
                         )

case class Issue(itunes_product_id: String)
object Issue {
  implicit val decodeJson: DecodeJson[Issue] = DecodeJson { (j: HCursor) =>
    val h = j --\ "issue"
    for {
      aa <- h.get[String]("itunes_product_id")
    } yield Issue(aa)
  }
}

case class IssueVersionCompact(s3Key: S3Key, updatedAt: Date)

object IssueVersionCompact {
  val decodeS3Key: DecodeJson[S3Key] = DecodeJson.StringDecodeJson.map(S3Key(_))
  val decodeIso8601Date: DecodeJson[Date] = DecodeJson.StringDecodeJson.map(Iso8601.parse(_))

  implicit val decodeJson: DecodeJson[IssueVersionCompact] = DecodeJson { (j: HCursor) =>
    val h = j --\ "issue_version"
    for {
      aa <- h.get("s3_key")(decodeS3Key)
      bb <- h.get("updated_at")(decodeIso8601Date)
    } yield IssueVersionCompact(aa, bb)
  }
}

object IssueVersion {
  implicit val decodeJson: DecodeJson[IssueVersion] = {
    DecodeJson.jdecode3L(IssueVersion.apply _)("s3_key", "itunes_product_id", "application_bundle_id")
  }
}

case class Dashboard(base: Request) {
  val encoder = URLEncoder.encode((_:String), "UTF-8")
  def issueVersionsForIssue(itp: String): Handler[List[IssueVersionCompact]] =
    (base / "issues" / itp / "issue_versions").as_str ~> {
      _.parseOption.map(_.fieldOrZero("issue_versions").jdecode[List[IssueVersionCompact]].value.getOrElse(Nil)).getOrElse(sys.error("Expected JSON, got something else parsing issue versions"))
    }

  def newIssueVersions(after: Date): Handler[List[IssueVersion]] =
    (base / "issue_versions" / "cdn" <<? Map("created_after" -> Iso8601.format(after))).as_str ~> {
      _.parseOption.map(_.arrayOrEmpty.flatMap(_.jdecode[IssueVersion].value)).getOrElse(sys.error("Expected JSON, got something else parsing issue versions"))
    }

  def issuesForApplication(applicationBundleId: String): Handler[List[Issue]] = {
    (base / "publications" / applicationBundleId / "issues").as_str ~> { v =>
      v.parseOption.map(_.fieldOrZero("issues").jdecode[List[Issue]].value.getOrElse(Nil)).getOrElse(sys.error("Expected JSON, got something else parsing issue versions"))
    }
  }

  def patch(itunesProductId: String, bundle_key: S3Key, patch_s3_key: S3Key, patch_md5: String): Handler[(Boolean, String)] =
    (base / "issues" / itunesProductId / "patch_issue_version" / encoder(bundle_key.s) << Map(
      "patch_md5" -> patch_md5, "patch_s3_key" -> patch_s3_key.s
    )).as_str ~> ((true, _))
}
