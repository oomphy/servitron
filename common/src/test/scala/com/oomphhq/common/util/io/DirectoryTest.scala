package com.oomphhq.common.util.io

import com.oomphhq.common.util.test.TestResource
import org.specs2.mutable.Specification
import com.oomphhq.common.util.io.Directory._
import java.io.{File => JavaFile}

final class DirectoryTest extends Specification with TestResource {
  "A directory" should {
    "is not contructable from a file" in {
      directory(testResourceFile("/f0d63f2df80b290f687d34cf2aa52d17.png").file) must throwA[Exception]
    }

    "is contructable from a directory" in {
      directory(packageRoot.file) must not(throwA[Exception])
    }

    "is contructable from a directory that does not exist" in {
      directory("/foo/bar") must not(throwA[Exception])
    }
  }

  "A non-existent directory" should {
    val dir = directory("/unzipped_content/empty_dir")

    "be contructable" in {
      dir must not(throwA[Exception])
    }

    "have no files" in {
      dir.listFiles() must beEmpty
    }

    "have no sub-directories" in {
      dir.listDirectories() must beEmpty
    }
  }

  "An null directory" should {
    "is not contructable" in {
      directory(null.asInstanceOf[JavaFile]) must throwAn[Exception]("null is not a directory")
      directory(null.asInstanceOf[String]) must throwAn[Exception]("null is not a directory")
      directory(directory("/foo/bar"), null) must throwAn[Exception]("Directory_\\(/foo/bar\\)/null is not a directory")
      directory(null, "whatever") must throwAn[Exception]("null/whatever is not a directory")
      directory(null, null) must throwAn[Exception]("null/null is not a directory")
    }
  }
}
