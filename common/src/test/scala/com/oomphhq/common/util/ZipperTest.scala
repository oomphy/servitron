package com.oomphhq.common.util

import org.specs2.mutable.Specification
import test.TestResource

final class ZipperTest extends Specification with TestResource {
  "Zipping an empty directory" should {
    "yields a zip with only directories in it" in {
      val src = testResourceDirectory("/unzipped_content/empty_dir")
      Io.withTempFile({ zip =>
        zip.out() { out =>
          Zipper.zip(out, ZipAddDir(src))
        }
        Io.withTempDir(tmp => {
          Unzipper.unzip(zip, tmp)
          tmp.listDirectories() must beEmpty
        })
      })
      "" must beEqualTo("")
    }
  }

  "Zipping a non-empty directory" should {
    "yields a valid zip file" in {
      val src = testResourceDirectory("/unzipped_content/aww_small")
      Io.withTempFile({ zip =>
        zip.out() { out =>
          Zipper.zip(out, ZipAddDir(src))
        }
        Io.withTempDir(tmp => {
          Unzipper.unzip(zip, tmp)
          val unzippedDirs = tmp.listDirectories()
          unzippedDirs(0).name must beEqualTo("aww_small")
          unzippedDirs(0).listDirectories()(0).name must beEqualTo("20-Contents")
          unzippedDirs(0).listFiles()(0).name must beEqualTo("cover.png")
          unzippedDirs(0).listFiles()(1).name must beEqualTo("SectionIndexLandscape.plist")
          unzippedDirs(0).listFiles()(2).name must beEqualTo("SectionIndexPortrait.plist")
          unzippedDirs(0).listDirectories()(0).listFiles()(0).name must beEqualTo("TP1.png")
        })
      })
      "" must beEqualTo("")
    }
  }
}
