package com.oomphhq.common.util

import org.specs2.mutable.Specification

final class DownloaderTest extends Specification {
  "Downloading a small file" should {
    "should download it correctly" in {
      Io.withTempFile {tmp =>
        Downloader.downloadToFile("https://en.gravatar.com/userimage/476729/f0d63f2df80b290f687d34cf2aa52d17.png", tmp)
        tmp.exists must beTrue
        tmp.in {s => Digester.md5(s) must beEqualTo("8f6c73bd2f041ee2deb19070f9d10336")}
      }
      "" must beEqualTo("")
    }
  }
}
