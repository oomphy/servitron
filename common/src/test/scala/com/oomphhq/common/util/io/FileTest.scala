package com.oomphhq.common.util.io

import org.specs2.mutable.Specification
import com.oomphhq.common.util.test.TestResource
import File._
import java.io.{File => JavaFile}
import com.oomphhq.common.util.Io, Io._

final class FileTest extends Specification with TestResource {
  "A file" should {
    "is not contructable from a directory" in {
      file("/") must throwA[Exception]
    }

    "is contructable from a file" in {
      testResourceFile("/f0d63f2df80b290f687d34cf2aa52d17.png") must not(throwA[Exception])
    }

    "can be moved" in {
      withTempFile { f =>
        val originalFile = testResourceFile("/f0d63f2df80b290f687d34cf2aa52d17.png")
        val originalSize = originalFile.file.length()
        originalFile.move(f).file.length() must beEqualTo(originalSize)
      }
      "" must beEqualTo("")
    }
  }

  "An null file" should {
    "is not contructable" in {
      file(null.asInstanceOf[String]) must throwAn[Exception]("null is not a file")
      file(null.asInstanceOf[JavaFile]) must throwAn[Exception]("null is not a file")
    }
  }

  "File extensions" should {
    val png = file("/bar/baz/foo.png")
    val jpeg = file("bar/foo.jpeg")
    val txt = file("foo.txt")
    val text = file("/foo.text")
    val noExt = file("f0d63f2df80b290f687d34cf2aa52d17")
    val dotFile = file(".gitignore")

    "should work" in {
      png.extension.get must beEqualTo("png")
      jpeg.extension.get must beEqualTo("jpeg")
      txt.extension.get must beEqualTo("txt")
      text.extension.get must beEqualTo("text")
      noExt.extension must beNone
      dotFile.extension must beNone
    }
  }
}
