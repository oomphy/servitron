package com.oomphhq.common.util

import org.specs2.mutable.Specification
import test.TestResource

final class UnzipperTest extends Specification with TestResource {
  "Unzipping with zip file containing only directories" should {
    "create only directories" in {
      Io.withTempDir({tmp =>
        val zip = testResourceFile("/zipped_content/just_dirs.zip")
        Unzipper.unzip(zip, tmp)
        val unzippedDirs = tmp.listDirectories()
        unzippedDirs.foreach {_.listDirectories() must not beEmpty}
        unzippedDirs must not beEmpty
      })
      "" must beEqualTo("")
    }
  }

  "Unzipping with zip file with files & one level of directories" should {
    "creates the zip directory structure" in {
      val zip = testResourceFile("/zipped_content/aww_small.zip")
      Io.withTempDir({tmp =>
        Unzipper.unzip(zip, tmp)
        val unzippedDirs = tmp.listDirectories()
        unzippedDirs(0).name must beEqualTo("aww_small")
        unzippedDirs(0).listDirectories()(0).name must beEqualTo("20-Contents")
        unzippedDirs(0).listFiles()(0).name must beEqualTo("cover.png")
        unzippedDirs(0).listFiles()(1).name must beEqualTo("SectionIndexLandscape.plist")
        unzippedDirs(0).listFiles()(2).name must beEqualTo("SectionIndexPortrait.plist")
        unzippedDirs(0).listDirectories()(0).listFiles()(0).name must beEqualTo("TP1.png")
      })
      "" must beEqualTo("")
    }
  }

  "Unzipping with zip file with files & two levels of directories" should {
    "creates the zip directory structure" in {
      val zip = testResourceFile("/zipped_content/aww_122011.zip")
      Io.withTempDir({tmp =>
        Unzipper.unzip(zip, tmp)
        val unzippedDirs = tmp.listDirectories()
        unzippedDirs(0).name must beEqualTo("aww_122011")
        unzippedDirs(0).listDirectories()(0).name must beEqualTo("20-Contents")
        unzippedDirs(0).listDirectories()(0).listFiles()(0).name must beEqualTo("L1-1.pdf")
        unzippedDirs(0).listDirectories()(0).listFiles()(1).name must beEqualTo("P1-1.pdf")
        unzippedDirs(0).listDirectories()(0).listFiles()(2).name must beEqualTo("TP1.png")
        unzippedDirs(0).listDirectories()(0).listDirectories()(0).name must beEqualTo("VIPS1")
        unzippedDirs(0).listDirectories()(0).listDirectories()(0).listFiles()(0).name must beEqualTo("S1-1.pdf")
      })
      "" must beEqualTo("")
    }
  }
}
