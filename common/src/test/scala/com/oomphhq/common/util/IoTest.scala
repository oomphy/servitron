package com.oomphhq.common.util

import org.specs2.mutable.Specification

final class IoTest extends Specification {
  "Creating a temporary directory" should {
    "create a directory in the system configured location" in {
      Io.withTempDir({tmp =>
        tmp.path must contain(sys.props("java.io.tmpdir"))
      })
      "" must beEqualTo("")
    }

    "must create a directory that exists" in {
      Io.withTempDir({tmp => tmp.exists must beTrue})
      "" must beEqualTo("")
    }
  }

  "Creating a temporary file" should {
    "create a file in the system configured location" in {
      Io.withTempFile({tmp =>
        tmp.path must contain(sys.props("java.io.tmpdir"))
      })
      "" must beEqualTo("")
    }

    "must create a file that exists" in {
      Io.withTempFile({tmp => tmp.exists must beTrue})
      "" must beEqualTo("")
    }
  }
}
