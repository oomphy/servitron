package com.oomphhq.common.util

import org.specs2.mutable.Specification
import test.TestResource
import java.io.ByteArrayInputStream

final class DigesterTest extends Specification with TestResource {
  "Digesting a string" should {
    val s = new ByteArrayInputStream("foo".getBytes)

    "produce a known" in {
      Digester.md5(s) must beEqualTo("acbd18db4cc2f85cedef654fccc4a4d8")
    }
  }

  "Digesting a known file" should {
    "produce a known md5" in {
      testResourceFile("/f0d63f2df80b290f687d34cf2aa52d17.png").in {s =>
        Digester.md5(s) must beEqualTo("8f6c73bd2f041ee2deb19070f9d10336")
      }
    }
    "" must beEqualTo("")
  }
}
