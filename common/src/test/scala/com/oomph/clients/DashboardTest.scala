package com.oomphhq.clients

import org.specs2.mutable.Specification
import com.oomph.clients.Dashboard
import dispatch.{url, Http}
import java.util.Date
import com.oomph.s3.S3Key

final class DashboardTest extends Specification {

  def withHttp[V](f: Http => V) = {
    val http = new Http
    val x = f(http)
    http.shutdown()
    x
  }

  "Pulling issue versions" should {
    "work" in {
      // payload: {"issue_versions":[{"issue_version":{"published":true,"fingerprint":"0846d4711949b9a4c2e9381caba25d3b","s3_key":"content_bundles/912ab9f3-f53a-44e9-a67b-0571f7e02268/koorong-demo.zip","has_patches":false,"magdata_version":15,"updated_at":"2012-07-13T06:00:28Z"}}]}
      val dashboard = new Dashboard(url("https://api.oomphhq.com/publish/api/v1").as_!("what", "foo"))
      withHttp(http => {
        http(dashboard.issueVersionsForIssue("com.mogeneration.oomphviewer.KoorongDemo")).headOption must beSome
        http(dashboard.newIssueVersions(new Date(60L * 60 * 24 * 365 * 40))).headOption must beSome
      })
    }
  }

  "issues" should {
    "work" in {
      val dashboard = new Dashboard(url("https://api.oomphhq.com/publish/api/v1").as_!("what", "foo"))
      val xs = withHttp(http => {
        http(dashboard.issuesForApplication("au.com.acpmagazines.gourmettraveller"))
      })
      xs.find(_.itunes_product_id == "au.com.acpmagazines.gourmettraveller.2012.february").must(beSome)
    }
  }

  "patching" should {
    "encode the s3 key" in {
      val dashboard = new Dashboard(url("http://localhost"))
      dashboard.patch("a", S3Key("1/2"), S3Key("abc"), "md5").request.path must_== "/issues/a/patch_issue_version/1%2F2"
    }
  }
}
