organization := "com.oomphhq"

name := "scala-commons"

version := "1.0.2-SNAPSHOT"

scalaVersion := "2.9.2"

libraryDependencies ++= Seq(
  "net.databinder" %% "dispatch-http" % "0.8.8" withSources(),
  "org.apache.commons" % "commons-compress" % "1.3" withSources(),
  "commons-io" % "commons-io" % "2.4" withSources(),
  "com.yammer.dropwizard" % "dropwizard-client" % "0.5.1",
  "javax.servlet" % "javax.servlet-api" % "3.0.1",
  "io.argonaut" %% "argonaut" % "6.0-RC2",
  "joda-time" % "joda-time" % "2.1",
  "org.joda" % "joda-convert" % "1.2",
  "org.specs2" %% "specs2" % "1.12" % "test" withSources(),
  "com.amazonaws" % "aws-java-sdk" % "1.3.20"
)

// Needed for dropwizard-client

ivyXML := <dependency org="org.eclipse.jetty.orbit" name="javax.servlet" rev="3.0.0.v201112011016">
  <artifact name="javax.servlet" type="orbit" ext="jar"/>
  </dependency>

exportJars := true 

resolvers += "Codahale's Repo" at "http://repo.codahale.com" // Dropwizard

resolvers += "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/" // Argonaut and scalaz
