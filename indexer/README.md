# Bundle Full Text Indexer

## Overview

Full text indexer for Oomph content bundles.

## Setup

    $ brew install sbt
    $ sbt
    > update

Note. All commands should be run from the root of the project, not the `indexer` directory.

## Generating an Index

    $ sbt
    > project indexer
    > indexer/run index-local <path to local zip file>

## Running Tests

Tests can be run all at once:

    $ sbt
    > test

For a single project:

    > project indexer
    > test

Or shorthand (like all sbt commands):

    > indexer/test

Single tests or groups or whatever you want can be run like:

    $ sbt
    > test-only com.oomphhq.indexer.extractors.pdf.*

SBT has docs on this: http://www.scala-sbt.org/0.13/docs/Testing.html

## IntelliJ Support

If you make changes to dependencies run this in an sbt session:

    > gen-idea

And accept the IDEA prompts to reload.

