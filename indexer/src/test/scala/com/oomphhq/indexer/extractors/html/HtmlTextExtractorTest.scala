package com.oomphhq.indexer.extractors.html

import org.specs2.mutable.Specification
import java.io._
import java.net.URL
import com.oomphhq.common.util.Io

final class HtmlTextExtractorTest extends Specification {
  "Parsing a complex HTML file" should {
    "be able to pull the text out" in {
      val source = new ByteArrayInputStream(Google.source.getBytes("UTF-8"))
      val output = new HtmlTextExtractor().extract("google.html", source)
      output must have size 1
      output(0).must(beEmpty).not
      output(0) must contain("I'm Feeling Lucky")
    }
  }

  "Pulling a file from the internet" should {
    "get text" in {
      val content = new URL("http://apple.com/").openStream()
      try {
        val output = new HtmlTextExtractor().extract("google.html", content)
        output must have size 1
        output(0).must(beEmpty).not
        output(0) must contain("Apple Inc. All rights reserved.")
      } finally {
        Io.close(content)
      }
    }
  }
}
