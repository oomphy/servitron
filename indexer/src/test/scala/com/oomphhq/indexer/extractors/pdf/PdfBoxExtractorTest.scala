package com.oomphhq.indexer.extractors.pdf

import com.oomphhq.common.util.test.TestResource
import com.oomphhq.indexer.test.NonLoggingSpecification
import org.specs2.mutable.Specification

final class PdfBoxExtractorTest extends NonLoggingSpecification with TestResource {
  "Parsing a PDF file" should {
    val extractor = new PdfBoxExtractor()

    "be able to pull the text out" in {
      val output = extractor.extract("", testResourceStream("/sample_content/misc/dodgy_ad/L1-1.pdf"))
      output must have size 1
      output(0).must(beEmpty).not
      output(0) must contain("X LITE SHORT")
      output(0) must contain("Short is light weight, breathable and moves freely")
      output(0) must contain("Super light weight short")
      output(0) must contain("FABRICS")
      output(0) must contain("COLOURS")
    }
  }
}
