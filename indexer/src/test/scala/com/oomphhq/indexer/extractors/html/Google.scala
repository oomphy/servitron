package com.oomphhq.indexer.extractors.html

object Google {
    val source =
      """
        |<!doctype html>
        |<html itemscope="itemscope" itemtype="http://schema.org/WebPage">
        |<head>
        |<meta itemprop="image" content="/images/google_favicon_128.png">
        |<title>Google</title>
        |</head>
        |<body class="hp"
        |      onload="try{if(!google.j.b){document.f&amp;&amp;document.f.q.focus();document.gbqf&amp;&amp;document.gbqf.q.focus();}}catch(e){}if(document.images)new Image().src='/images/nav_logo129.png'"
        |      alink="#dd4b39" bgcolor="#fff" id="gsr" link="#12c" text="#222" vlink="#61c">
        |<div id="pocs" style="display:none;position:absolute">
        |    <div id="pocs0"><span><span>Google</span> Instant is unavailable. Press Enter to search.</span>&nbsp;<a
        |            href="/support/websearch/bin/answer.py?answer=186645&amp;form=bb&amp;hl=en-AU">Learn more</a></div>
        |    <div id="pocs1"><span>Google</span> Instant is off due to connection speed. Press Enter to search.</div>
        |    <div id="pocs2">Press Enter to search.</div>
        |</div>
        |<div id="cst">
        |    <div style="display:none">&nbsp;</div>
        |</div>
        |<a href="/setprefs?prev=https://www.google.com.au/&amp;sig=0_QbD-cXI_I1ZzfM3JUmDb01bWl20%3D&amp;suggon=2"
        |   style="left:-1000em;position:absolute">Screen-reader users, click here to turn off Google Instant.</a> <textarea
        |        name="csi" id="csi" style="display:none"></textarea>
        |<script>if(google.j.b)document.body.style.visibility='hidden';</script>
        |<div id="mngb">
        |<div id=gb>
        |<script>window.gbar&&gbar.eli&&gbar.eli()</script>
        |<div id=gbw>
        |<div id=gbzw>
        |    <div id=gbz><span class=gbtcb></span>
        |        <ol id=gbzc class=gbtc>
        |            <li class=gbt><a onclick=gbar.logger.il(1,{t:119}); class=gbzt id=gb_119
        |                             href="https://plus.google.com/u/0/?tab=wX"><span class=gbtb2></span><span
        |                    class=gbts>+Tom</span></a></li>
        |            <li class=gbt><a onclick=gbar.logger.il(1,{t:1}); class="gbzt gbz0l gbp1" id=gb_1
        |                             href="https://www.google.com.au/webhp?hl=en&tab=ww&authuser=0"><span
        |                    class=gbtb2></span><span class=gbts>Search</span></a></li>
        |            <li class=gbt><a onclick=gbar.qs(this);gbar.logger.il(1,{t:2}); class=gbzt id=gb_2
        |                             href="https://www.google.com.au/imghp?hl=en&tab=wi&authuser=0"><span
        |                    class=gbtb2></span><span class=gbts>Images</span></a></li>
        |            <li class=gbt><a onclick=gbar.logger.il(1,{t:23}); class=gbzt id=gb_23
        |                             href="https://mail.google.com/mail/?tab=wm&authuser=0"><span
        |                    class=gbtb2></span><span class=gbts>Mail</span></a></li>
        |            <li class=gbt><a onclick=gbar.logger.il(1,{t:25}); class=gbzt id=gb_25
        |                             href="https://drive.google.com/?tab=wo&authuser=0"><span
        |                    class=gbtb2></span><span class=gbts>Drive</span></a></li>
        |            <li class=gbt><a onclick=gbar.logger.il(1,{t:24}); class=gbzt id=gb_24
        |                             href="https://www.google.com/calendar?tab=wc&authuser=0"><span
        |                    class=gbtb2></span><span class=gbts>Calendar</span></a></li>
        |            <li class=gbt><a onclick=gbar.logger.il(1,{t:38}); class=gbzt id=gb_38
        |                             href="https://sites.google.com/?tab=w3&authuser=0"><span
        |                    class=gbtb2></span><span class=gbts>Sites</span></a></li>
        |            <li class=gbt><a onclick=gbar.logger.il(1,{t:53}); class=gbzt id=gb_53
        |                             href="https://www.google.com/contacts/?hl=en&tab=wC&authuser=0"><span
        |                    class=gbtb2></span><span class=gbts>Contacts</span></a></li>
        |            <li class=gbt><a onclick=gbar.qs(this);gbar.logger.il(1,{t:8}); class=gbzt id=gb_8
        |                             href="https://maps.google.com.au/maps?hl=en&tab=wl&authuser=0"><span
        |                    class=gbtb2></span><span class=gbts>Maps</span></a></li>
        |            <li class=gbt><a class=gbgt id=gbztm href="http://www.google.com.au/intl/en/options/"
        |                             onclick="gbar.tg(event,this)" aria-haspopup=true aria-owns=gbd><span
        |                    class=gbtb2></span><span id=gbztms class="gbts gbtsa"><span id=gbztms1>More</span><span
        |                    class=gbma></span></span></a>
        |
        |                <div class=gbm id=gbd aria-owner=gbztm>
        |                    <div id=gbmmb class="gbmc gbsb gbsbis">
        |                        <ol id=gbmm class="gbmcc gbsbic">
        |                            <li class=gbmtc><a onclick=gbar.qs(this);gbar.logger.il(1,{t:36}); class=gbmt
        |                                               id=gb_36
        |                                               href="https://www.youtube.com/?gl=AU&tab=w1&authuser=0">YouTube</a>
        |                            </li>
        |                            <li class=gbmtc><a onclick=gbar.logger.il(1,{t:5}); class=gbmt id=gb_5
        |                                               href="https://news.google.com.au/nwshp?hl=en&tab=wn&authuser=0">News</a>
        |                            </li>
        |                            <li class=gbmtc><a onclick=gbar.qs(this);gbar.logger.il(1,{t:51}); class=gbmt
        |                                               id=gb_51
        |                                               href="https://translate.google.com.au/?hl=en&tab=wT&authuser=0">Translate</a>
        |                            </li>
        |                            <li class=gbmtc><a onclick=gbar.qs(this);gbar.logger.il(1,{t:10}); class=gbmt
        |                                               id=gb_10
        |                                               href="http://books.google.com.au/bkshp?hl=en&tab=wp&authuser=0">Books</a>
        |                            </li>
        |                            <li class=gbmtc><a onclick=gbar.qs(this);gbar.logger.il(1,{t:6}); class=gbmt
        |                                               id=gb_6
        |                                               href="http://www.google.com.au/shopping?hl=en&tab=wf&authuser=0">Shopping</a>
        |                            </li>
        |                            <li class=gbmtc><a onclick=gbar.logger.il(1,{t:30}); class=gbmt id=gb_30
        |                                               href="https://www.blogger.com/?tab=wj">Blogger</a></li>
        |                            <li class=gbmtc><a onclick=gbar.qs(this);gbar.logger.il(1,{t:31}); class=gbmt
        |                                               id=gb_31 href="https://plus.google.com/u/0/photos?tab=wq">Photos</a>
        |                            </li>
        |                            <li class=gbmtc>
        |                                <div class="gbmt gbmh"></div>
        |                            </li>
        |                            <li class=gbmtc><a onclick=gbar.logger.il(1,{t:66});
        |                                               href="http://www.google.com.au/intl/en/options/" class=gbmt>Even
        |                                more</a></li>
        |                        </ol>
        |                        <div class=gbsbt></div>
        |                        <div class=gbsbb></div>
        |                    </div>
        |                </div>
        |            </li>
        |        </ol>
        |    </div>
        |</div>
        |<div id=gbq>
        |    <div id=gbq1 class="gbt gbqfh"><a class=gbqla href="/webhp?hl=en&tab=ww&authuser=0"
        |                                      onclick="gbar.logger.il(39)" title="Go to Google Home">
        |        <table id=gbqlt>
        |            <tr>
        |                <td id=gbqlw class=gbgt><span id=gbql></span></td>
        |            </tr>
        |        </table>
        |        <div class=gbqlca></div>
        |    </a></div>
        |    <div id=gbq2 class="gbt gbqfh">
        |        <div id=gbqfw>
        |            <form id=gbqf name=gbqf method=get action="/search" onsubmit="gbar.logger.il(31);">
        |                <fieldset class=gbxx>
        |                    <legend class=gbxx>Hidden fields</legend>
        |                    <div id=gbqffd><input type=hidden name="output" value="search"><input type=hidden
        |                                                                                          name="sclient"
        |                                                                                          value="psy-ab">
        |                    </div>
        |                </fieldset>
        |                <fieldset class=gbqff id=gbqff>
        |                    <legend class=gbxx></legend>
        |                    <div id=gbfwa class="gbqfwa ">
        |                        <div id=gbqfqw class=gbqfqw>
        |                            <div id=gbqfqwb class=gbqfqwc><input id=gbqfq class=gbqfif name=q type=text
        |                                                                 autocomplete=off value=""></div>
        |                        </div>
        |                    </div>
        |                </fieldset>
        |                <div id=gbqfbw>
        |                    <button id=gbqfb aria-label="Google Search" class=gbqfb name=btnG><span
        |                            class=gbqfi></span></button>
        |                </div>
        |                <div id=gbqfbwa class=jsb>
        |                    <button id=gbqfba aria-label="Google Search" name=btnK class=gbqfba><span id=gbqfsa>Google Search</span>
        |                    </button>
        |                    <button id=gbqfbb aria-label="I'm Feeling Lucky" name=btnI class=gbqfba
        |                            onclick="if(this.form.q.value)this.checked=1;else window.top.location='/doodles/'">
        |                        <span id=gbqfsb>I'm Feeling Lucky</span></button>
        |                </div>
        |            </form>
        |        </div>
        |    </div>
        |</div>
        |<div id=gbu>
        |    <div id=gbvg class=gbvg><h2 class=gbxx>Account Options</h2><span class=gbtcb></span>
        |        <ol class="gbtc gbsr">
        |            <li class=gbt><a class=gbgt id=gbg6 href="https://plus.google.com/u/0/me?tab=wX&authuser=0"
        |                             onclick="gbar.tg(event,document.getElementById('gbg4'))" tabindex=-1
        |                             aria-haspopup=true aria-owns=gbd4><span
        |                    id=gbi4t>tom@mogeneration.com</span></a></li>
        |            <li class="gbt gbtn"><a class="gbgt gbgtd gb_gbnh " id=gbg1
        |                                    href="https://plus.google.com/u/0/notifications/all?hl=en"
        |                                    title="Notifications" onclick="gbar.tg(event,this)" aria-haspopup=true
        |                                    aria-owns=gbd1><span id=gbgs1 class=gbg1t><span id=gbi1a
        |                                                                                    class=gbid></span><span
        |                    id=gbi1 class=gbids>&nbsp;</span></span><span class=gbmab></span><span
        |                    class=gbmac></span></a>
        |
        |                <div id=gbd1 class="gbm gbmsgo" aria-owner=gbg1>
        |                    <div class=gbmc></div>
        |                    <div class=gbmsg></div>
        |                </div>
        |            </li>
        |            <li class="gbt gbtsb"><a class=gbgt id=gbg3 href="https://plus.google.com/u/0/stream/all?hl=en"
        |                                     onclick="gbar.tg(event,this)" aria-haspopup=true aria-owns=gbd3>
        |                <div id=gbgs3><span class=gbmab></span><span class=gbmac></span><span id=gbgsi></span><span
        |                        id=gbgss>&nbsp;</span><span id=gbi3>Share</span><span id=gbgsa></span></div>
        |            </a>
        |
        |                <div class="gbm gbmsgo" id=gbd3 aria-owner=gbg3>
        |                    <div class=gbmc></div>
        |                    <div class=gbmsg></div>
        |                </div>
        |            </li>
        |            <li class=gbt guidedhelpid=gbacsw><a class="gbgt gbg4p" id=gbg4
        |                                                 href="https://plus.google.com/u/0/me?tab=wX&authuser=0"
        |                                                 onclick="gbar.tg(event,this)" aria-haspopup=true
        |                                                 aria-owns=gbd4><span id=gbgs4><img id=gbi4i width=27
        |                                                                                    height=27
        |                                                                                    onerror="window.gbar&&gbar.pge?gbar.pge():this.loadError=1;"
        |                                                                                    src="//lh5.googleusercontent.com/-XUB1LmbQLto/AAAAAAAAAAI/AAAAAAAAAAA/sL7iBleD2I8/s27-c/photo.jpg"
        |                                                                                    alt="Tom Adams"><img
        |                    id=gbi4ip style=display:none><span id=gbi4id style="display:none"></span><span
        |                    class=gbmai></span><span class=gbmab></span><span class=gbmac></span></span></a>
        |
        |                <div class=gbm id=gbd4 aria-owner=gbg4 guidedhelpid=gbd4>
        |                    <div class=gbmc>
        |                        <div id=gbmpdv>
        |                            <div class=gbpmc>
        |                                <table id=gbpm>
        |                                    <tr>
        |                                        <td class=gbpmtc>
        |                                            <div id=gbpms>This account is managed by <span class=gbpms2>mogeneration.com</span>.
        |                                            </div>
        |                                            <a target=_blank class=gbml1
        |                                               href="http://www.google.com/support/accounts/bin/answer.py?answer=181692&hl=en">Learn
        |                                                more</a></td>
        |                                    </tr>
        |                                </table>
        |                            </div>
        |                            <div id=gbmpiw><a class="gbmpiaa gbp1" onclick="gbar.logger.il(10,{t:146})"
        |                                              href="https://plus.google.com/u/0/me?tab=wX&authuser=0"><span
        |                                    id=gbmpid style="display:none"></span><img id=gbmpi width=96 height=96
        |                                                                               onerror="window.gbar&&gbar.ppe?gbar.ppe():this.loadError=1;"
        |                                                                               src="//lh5.googleusercontent.com/-XUB1LmbQLto/AAAAAAAAAAI/AAAAAAAAAAA/sL7iBleD2I8/s27-c/photo.jpg"
        |                                                                               alt="Tom Adams"></a><span
        |                                    id=gbmpicb><span class=gbxv>Change photo</span></span><a
        |                                    href="https://plus.google.com/u/0/me?tab=wX&authuser=0" id=gbmpicp
        |                                    onclick="gbar.i.e(event)">Change photo</a></div>
        |                            <div class=gbpc><span id=gbmpn class=gbps onclick="gbar.logger.il(10,{t:69})">Tom Adams</span><span
        |                                    class=gbps2>tom@mogeneration.com</span>
        |
        |                                <div class=gbmlbw><a id=gb_156 onclick="gbar.logger.il(10,{t:156})"
        |                                                     href="https://www.google.com/settings?ref=home&authuser=0"
        |                                                     class=gbmlb>Account</a>&ndash;<a
        |                                        onclick="gbar.logger.il(10,{t:156})"
        |                                        href="https://www.google.com/settings/privacy?tab=4&authuser=0"
        |                                        class=gbmlb>Privacy</a></div>
        |                                <a role=button id=gbmplp onclick="gbar.logger.il(10,{t:146})"
        |                                   href="https://plus.google.com/u/0/me?tab=wX&authuser=0"
        |                                   class="gbqfb gbiba gbp1">View profile</a></div>
        |                        </div>
        |                        <div id=gbmps>
        |                            <div id=gbmpasb class='gbsb gbsbis'>
        |                                <div id=gbmpas class=gbsbic>
        |                                    <div id=gbmpm_0 class="gbmtc gbp0"><a id=gbmpm_0_l
        |                                                                          href="https://www.google.com.au/webhp?authuser=0"
        |                                                                          class=gbmt><span
        |                                            class="gbmpiaw gbxv"><img class=gbmpia width=48 height=48
        |                                                                      onerror="window.gbar&&gbar.pae?gbar.pae(this):this.loadError=1;"
        |                                                                      data-asrc="//lh5.googleusercontent.com/-XUB1LmbQLto/AAAAAAAAAAI/AAAAAAAAAAA/sL7iBleD2I8/s48-c/photo.jpg"
        |                                                                      alt="Tom Adams"></span><span
        |                                            class=gbmpnw><span class=gbps>Tom Adams</span><span class=gbps2><span
        |                                            class=gbps3>tom@mogeneration.com</span> (default)</span></span></a>
        |                                    </div>
        |                                    <div id=gbmpm_1 class=gbmtc><a id=gbmpm_1_l
        |                                                                   href="https://www.google.com.au/webhp?authuser=1"
        |                                                                   class=gbmt><span
        |                                            class="gbmpiaw gbxv"><img class=gbmpia width=48 height=48
        |                                                                      onerror="window.gbar&&gbar.pae?gbar.pae(this):this.loadError=1;"
        |                                                                      data-asrc="//lh6.googleusercontent.com/-jWkAHG0Bx9I/AAAAAAAAAAI/AAAAAAAAAAA/Ga_1t8VPyT0/s48-c/photo.jpg"
        |                                                                      alt="Tom Adams"></span><span
        |                                            class=gbmpnw><span class=gbps>Tom Adams</span><span class=gbps2><span
        |                                            class=gbps3>tomjadams@gmail.com</span></span></span></a></div>
        |                                </div>
        |                                <div class=gbsbt></div>
        |                                <div class=gbsbb></div>
        |                            </div>
        |                            <div id=gbmppc class="gbxx gbmtc"><a class=gbmt
        |                                                                 href="https://plus.google.com/u/0/dashboard"><span
        |                                    class=gbmppci></span>All your Google+ pages &rsaquo;</a></div>
        |                        </div>
        |                        <table id=gbmpal>
        |                            <tr>
        |                                <td class=gbmpala><a role=button
        |                                                     href="https://accounts.google.com/AddSession?hl=en&continue=https://www.google.com.au/"
        |                                                     class=gbqfbb>Add account</a></td>
        |                                <td class=gbmpalb><a target=_top role=button id=gb_71
        |                                                     onclick="gbar.logger.il(9,{l:'o'})"
        |                                                     href="https://accounts.google.com/Logout?hl=en&continue=https://www.google.com.au/"
        |                                                     class=gbqfbb>Sign out</a></td>
        |                            </tr>
        |                        </table>
        |                    </div>
        |                </div>
        |            </li>
        |            <noscript>
        |                <li class=gbt><a id=gbg7
        |                                 href="https://accounts.google.com/Logout?hl=en&continue=https://www.google.com.au/"
        |                                 class=gbgt><span class=gbgs><span class=gbit>Sign out</span></span></a>
        |                </li>
        |            </noscript>
        |            <div style="display:none">
        |                <div class=gbm id=gbd5 aria-owner=gbg5>
        |                    <div class=gbmc>
        |                        <ol id=gbom class=gbmcc>
        |                            <li class="gbkc gbmtc"><a class=gbmt href="/preferences?hl=en">Search
        |                                settings</a></li>
        |                            <li class=gbmtc>
        |                                <div class="gbmt gbmh"></div>
        |                            </li>
        |                            <li class="gbe gbmtc"><a id=gmlas class=gbmt href="/advanced_search?hl=en">Advanced
        |                                search</a></li>
        |                            <li class="gbe gbmtc"><a class=gbmt href="/language_tools?hl=en">Language
        |                                tools</a></li>
        |                            <li class=gbmtc>
        |                                <div class="gbmt gbmh"></div>
        |                            </li>
        |                            <li class="gbkp gbmtc"><a class=gbmt
        |                                                      href="https://www.google.com/history/?hl=en&authuser=0">Web
        |                                History</a></li>
        |                        </ol>
        |                    </div>
        |                </div>
        |            </div>
        |        </ol>
        |        <div id=gbdw></div>
        |    </div>
        |</div>
        |</div>
        |<div id=gbx1 class="gbqfh"></div>
        |<div id=gbx3></div>
        |<div id=gbbw>
        |    <div id=gbb></div>
        |</div>
        |<script>window.gbar&&gbar.elp&&gbar.elp()</script>
        |</div>
        |</div>
        |<textarea name="wgjc" id="wgjc" style="display:none"></textarea><textarea name="wgjs" id="wgjs"
        |                                                                          style="display:none"></textarea><textarea
        |        name="wgju" id="wgju" style="display:none"></textarea><textarea name="hcache" id="hcache"
        |                                                                        style="display:none"></textarea>
        |
        |<div id="main"><span class="ctr-p" id="body"><center><span id="prt" style="display:block"><div>
        |    <style>.pmoabs{background-color:#fff;border:1px solid
        |        #E5E5E5;color:#666;font-size:13px;padding-bottom:20px;position:absolute;right:2px;top:3px;z-index:986}.kd-button-submit{border:1px
        |        solid #3079ed;background-color:#4d90fe;background-image:-webkit-gradient(linear,left top,left
        |        bottom,from(#4d90fe),to(#4787ed));background-image:
        |        -webkit-linear-gradient(top,#4d90fe,#4787ed);background-image:
        |        -moz-linear-gradient(top,#4d90fe,#4787ed);background-image:
        |        -ms-linear-gradient(top,#4d90fe,#4787ed);background-image:
        |        -o-linear-gradient(top,#4d90fe,#4787ed);background-image:
        |        linear-gradient(top,#4d90fe,#4787ed);filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#4d90fe',EndColorStr='#4787ed')}.kd-button-submit:hover{border:1px
        |        solid #2f5bb7;background-color:#357ae8;background-image:-webkit-gradient(linear,left top,left
        |        bottom,from(#4d90fe),to(#357ae8));background-image:
        |        -webkit-linear-gradient(top,#4d90fe,#357ae8);background-image:
        |        -moz-linear-gradient(top,#4d90fe,#357ae8);background-image:
        |        -ms-linear-gradient(top,#4d90fe,#357ae8);background-image:
        |        -o-linear-gradient(top,#4d90fe,#357ae8);background-image:
        |        linear-gradient(top,#4d90fe,#357ae8);filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#4d90fe',EndColorStr='#357ae8')}.kd-button-submit:active{-webkit-box-shadow:inset
        |        0 1px 2px rgba(0,0,0,0.3);-moz-box-shadow:inset 0 1px 2px rgba(0,0,0,0.3);box-shadow:inset 0 1px 2px
        |        rgba(0,0,0,0.3)}.xbtn{color:#999;cursor:pointer;font-size:23px;line-height:5px;padding-top:5px}.padi{padding:0
        |        8px 0 10px}.padt{padding:5px 20px 0
        |        0;color:#444}.pads{text-align:left}#pmolnk{border-radius:2px;-moz-border-radius:2px;-webkit-border-radius:2px}#pmolnk
        |        a{color:#fff;display:inline-block;font-weight:bold;padding:5px 20px;text-decoration:none;white-space:nowrap}
        |    </style>
        |    <div class="pmoabs" id="pmocntr2" style="behavior:url(#default#userdata);display:none">
        |        <table border="0">
        |            <tr>
        |                <td colspan="2">
        |                    <div class="xbtn" onclick="google.promos&&google.promos.toast&& google.promos.toast.cpc()"
        |                         style="float:right">&times;</div>
        |                </td>
        |            </tr>
        |            <tr>
        |                <td class="padi" rowspan="2"><img src="/images/icons/product/chrome-48.png"></td>
        |                <td class="pads">A faster way to browse the web</td>
        |            </tr>
        |            <tr>
        |                <td class="padt">
        |                    <div class="kd-button-submit" id="pmolnk"><a
        |                            href="/chrome/index.html?hl=en&amp;brand=CHNG&amp;utm_source=en-hpp&amp;utm_medium=hpp&amp;utm_campaign=en"
        |                            onclick="google.promos&&google.promos.toast&& google.promos.toast.cl()">Install Google
        |                        Chrome</a></div>
        |                </td>
        |            </tr>
        |        </table>
        |    </div>
        |    <script type="text/javascript">(function(){var
        |        a={v:"a",w:"c",i:"d",k:"h",g:"i",K:"n",Q:"x",H:"ma",I:"mc",J:"mi",A:"pa",B:"pc",D:"pi",G:"pn",F:"px",C:"pd",L:"gpa",N:"gpi",O:"gpn",P:"gpx",M:"gpd"};var
        |        c={o:"hplogo",s:"pmocntr2"},e,g,k=document.getElementById(c.s);google.promos=google.promos||{};google.promos.toast=google.promos.toast||{};function
        |        l(b){k&&(k.style.display=b?"":"none",k.parentNode&&(k.parentNode.style.position=b?"relative":""))}function
        |        m(b){try{if(k&&b&&b.es&&b.es.m){var
        |        d=window.gbar.rtl(document.body)?"left":"right";k.style[d]=b.es.m-16+2+"px";k.style.top="20px"}}catch(f){google.ml(f,!1,{cause:e+"_PT"})}}
        |        google.promos.toast.cl=function(){try{window.gbar.up.sl(g,e,a.k,void
        |        0,1)}catch(b){google.ml(b,!1,{cause:e+"_CL"})}};google.promos.toast.cpc=function(){try{k&&(l(!1),window.gbar.up.spd(k,c.a,1,!0),window.gbar.up.sl(g,e,a.i,void
        |        0,1))}catch(b){google.ml(b,!1,{cause:e+"_CPC"})}};google.promos.toast.hideOnSmallWindow_=function(){try{if(k){var
        |        b=276,d=document.getElementById(c.o);d&&(b=Math.max(b,d.offsetWidth));var
        |        f=parseInt(k.style.right,10)||0;k.style.visibility=2*(k.offsetWidth+f)+b>document.body.clientWidth?"hidden":""}}catch(h){google.ml(h,!1,{cause:e+"_HOSW"})}};function
        |        q(){var b=["gpd","spd","aeh","sl"];if(!window.gbar||!window.gbar.up)return!1;for(var d=0,f;f=b[d];d++)if(!(f in
        |        window.gbar.up))return!1;return!0}
        |        google.promos.toast.init=function(b,d,f,h,n){try{if(!q())google.ml(Error("apa"),!1,{cause:e+"_INIT"});else
        |        if(k){window.gbar.up.aeh(window,"resize",google.promos.toast.hideOnSmallWindow_);window.lol=google.promos.toast.hideOnSmallWindow_;c.d="toast_count_"+d+(h?"_"+h:"");c.a="toast_dp_"+d+(n?"_"+n:"");e=f;g=b;var
        |        p=window.gbar.up.gpd(k,c.d,!0)||0;window.gbar.up.gpd(k,c.a,!0)||25<p
        |        ||k.currentStyle&&"absolute"!=k.currentStyle.position?l(!1):(window.gbar.up.spd(k,c.d,++p,!0),window.gbar.elr&&m(window.gbar.elr()),window.gbar.elc&&window.gbar.elc(m),l(!0),window.gbar.up.sl(g,e,a.g))}}catch(r){google.ml(r,!1,{cause:e+"_INIT"})}};})();
        |    </script>
        |    <script type="text/javascript">(function(){var sourceWebappPromoID=144002;var sourceWebappGroupID=5;var
        |        payloadType=5;window.gbar&&gbar.up&&gbar.up.r&&gbar.up.r(payloadType,function(show){if
        |        (show){google.promos.toast.init(sourceWebappPromoID,sourceWebappGroupID,payloadType,'0612');}
        |        });})();
        |    </script>
        |</div></span>
        |
        |    <div id="lga" style="height:231px;margin-top:-22px">
        |        <div style="padding-top:112px">
        |            <div title="Google" align="left" id="hplogo" onload="window.lol&&lol()"
        |                 style="background:url(images/srpr/logo4w.png) no-repeat;background-size:275px 95px;height:95px;width:275px">
        |                <div nowrap="nowrap"
        |                     style="color:#777;font-size:16px;font-weight:bold;position:relative;left:215px;top:70px">Australia
        |                </div>
        |            </div>
        |        </div>
        |    </div>
        |    <div style="height:102px"></div>
        |    <div id="prm-pt" style="font-size:83%;min-height:3.5em"><br>
        |        <script>window.gbar&&gbar.up&&gbar.up.tp&&gbar.up.tp();</script>
        |    </div>
        |</center></span>
        |
        |    <div class="ctr-p" id="footer">
        |        <div>
        |            <div id="ftby">
        |                <div id="fll">
        |                    <div id="flls"><a href="/intl/en/ads/">Advertising&nbsp;Programmes</a>‎<a href="/services/">Business
        |                        Solutions</a>‎<a href="/intl/en/policies/">Privacy & Terms</a>‎
        |                    </div>
        |                    <div id="flrs"><a href="https://plus.google.com/115477067087672475993"
        |                                      rel="publisher">+Google</a>‎<a href="/intl/en/about.html">About Google</a>‎<a
        |                            href="https://www.google.com.au/setprefdomain?prefdom=US&amp;sig=0_YoDYDNLRnVlvq7vctxvm5gbbKSc%3D"
        |                            id="fehl">Google.com</a>‎
        |                    </div>
        |                </div>
        |                <div id="flci"></div>
        |            </div>
        |        </div>
        |    </div>
        |</div>
        |<script>(function(){var _co='[\x22body\x22,\x22footer\x22,\x22xjsi\x22]';var _mstr='\x3cspan class\x3dctr-p
        |    id\x3dbody\x3e\x3c/span\x3e\x3cspan class\x3dctr-p id\x3dfooter\x3e\x3c/span\x3e\x3cspan
        |    id\x3dxjsi\x3e\x3c/span\x3e';function _gjp(){!(location.hash && _gjuc())&& setTimeout(_gjp,500);}
        |    var _coarr = eval('(' + _co + ')');google.j[1]={cc:[],co:_coarr,bl:['mngb','gb_'],funcs:[
        |    {'n':'pcs','i':'gstyle','css':document.getElementById('gstyle').innerHTML,'is':'','r':true,'sc':true},{'n':'pc','i':'cst','h':document.getElementById('cst').innerHTML,'is':'','r':true,'sc':true},{'n':'pc','i':'main','h':_mstr,'is':'','r':true,'sc':true}]
        |    };})();
        |</script>
        |<script data-url="/extern_chrome/67d41e199b1a81b0.js?bav=or.r_qf" id="ecs">function wgjp(){var
        |    xjs=document.createElement('script');xjs.src=document.getElementById('ecs').getAttribute('data-url');(document.getElementById('xjsd')||
        |    document.body).appendChild(xjs);};
        |</script>
        |<div id=xjsd></div>
        |<div id=xjsi>
        |    <script>if(google.y)google.y.first=[];(function(){function b(a){window.setTimeout(function(){var
        |        c=document.createElement("script");c.src=a;document.getElementById("xjsd").appendChild(c)},0)}google.dljp=function(a){google.xjsi||(google.xjsu=a,b(a))};google.dlj=b;})();
        |        if(!google.xjs){window._=window._||{};window._._DumpException=function(e){throw
        |        e};if(google.timers&&google.timers.load.t){google.timers.load.t.xjsls=new
        |        Date().getTime();}google.dljp('/xjs/_/js/k\x3dxjs.s.en_US.YqUxCLtYEL8.O/m\x3dc,sb,cr,cdos,jp,vm,tbui,mb,wobnm,cfm,abd,bihu,kp,lu,m,tnv,amcl,erh,hv,lc,ob,r,rsn,sf,sfa,shb,srl,tbpr,hsm,j,p,pcc,csi/am\x3dwA/rt\x3dj/d\x3d1/sv\x3d1/rs\x3dAItRSTNRnwe8AiYSDNdVCGn6RIvYNPrNkQ');google.xjs=1;}google.pmc={"c":{},"sb":{"agen":false,"cgen":true,"client":"hp","dh":true,"ds":"","eqch":true,"fl":true,"host":"google.com.au","jsonp":true,"lyrs":29,"msgs":{"lcky":"I\u0026#39;m
        |        Feeling Lucky","lml":"Learn more","oskt":"Input tools","psrc":"This search was removed from your \u003Ca
        |        href=\"/history\"\u003EWeb History\u003C/a\u003E","psrl":"Remove","sbit":"Search by image","srch":"Google
        |        Search"},"ovr":{"ejp":1,"ent":1,"ms":1},"pq":"","psy":"p","qcpw":false,"scd":10,"sce":4,"stok":"QazyiZiwJPPgNvbMwjIujglYagw","token":"c52mv0NOQety0K-2J7W69Q"},"cr":{"eup":false,"qir":false,"rctj":true,"ref":true,"uff":false},"cdos":{"bih":764,"biw":1440,"dima":"b"},"gf":{"pid":196,"si":true},"jp":{"mcr":5},"vm":{"bv":47883778,"d":"dGI","tc":true,"te":true,"tk":true,"ts":true},"tbui":{"dfi":{"am":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"df":["EEEE,
        |        d MMMM y","d MMMM y","d MMM
        |        y","d/M/yyyy"],"fdow":6,"nw":["S","M","T","W","T","F","S"],"wm":["January","February","March","April","May","June","July","August","September","October","November","December"]},"g":28,"k":true,"m":{"app":true,"bks":true,"blg":true,"dsc":true,"flm":true,"frm":true,"isch":true,"klg":true,"map":true,"mobile":true,"nws":true,"plcs":true,"ppl":true,"prc":true,"pts":true,"rcp":true,"shop":true,"vid":true},"t":null},"mb":{"db":false,"m_errors":{"default":"\u003Cfont
        |        color=red\u003EError:\u003C/font\u003E The server could not complete your request. Try again in 30
        |        seconds."},"m_tip":"Click for more information","nlpm":"-153px -84px","nlpp":"-153px
        |        -70px","utp":true},"wobnm":{},"cfm":{"data_url":"/m/financedata?output=search\u0026source=mus"},"abd":{"abd":false,"deb":false,"der":false,"det":false,"psa":false,"sup":false},"adp":{},"adp":{},"llc":{"carmode":"list","cns":false,"fling_time":300,"float":true,"hot":false,"ime":true,"mpi":0,"oq":"","p":true,"sticky":true,"t":false,"udp":600,"uds":600,"udt":600,"urs":false,"usr":true},"rkab":{"bl":"Feedback
        |        / More info","db":"Reported","di":"Thank you.","dl":"Report another problem","rb":"Wrong?","ri":"Please report
        |        the problem.","rl":"Cancel"},"bihu":{"MESSAGES":{"msg_img_from":"Image from %1$s","msg_ms":"More
        |        sizes","msg_si":"Similar"}},"riu":{"cnfrm":"Reported","prmpt":"Report"},"rmcl":{"bl":"Feedback / More
        |        info","db":"Reported","di":"Thank you.","dl":"Report another problem","rb":"Wrong?","ri":"Please report the
        |        problem.","rl":"Cancel"},"an":{},"kp":{"use_top_media_styles":true},"rk":{"bl":"Feedback / More
        |        info","db":"Reported","di":"Thank you.","dl":"Report another problem","efe":false,"rb":"Wrong?","ri":"Please
        |        report the
        |        problem.","rl":"Cancel"},"lu":{"cm_hov":true,"tt_kft":true,"uab":true},"m":{"ab":{"on":true},"ajax":{"gl":"au","hl":"en","q":""},"css":{"adpbc":"#fec","adpc":"#fffbf2","def":false,"showTopNav":true},"elastic":{"js":true,"rhs4Col":1072,"rhs5Col":1160,"rhsOn":true,"tiny":false},"exp":{"lru":true,"tnav":true},"kfe":{"adsClientId":33,"clientId":29,"kfeHost":"clients1.google.com.au","kfeUrlPrefix":"/webpagethumbnail?r=4\u0026f=3\u0026s=400:585\u0026query=\u0026hl=en\u0026gl=au","vsH":585,"vsW":400},"msgs":{"details":"Result
        |        details","hPers":"Hide personal results","hPersD":"Currently hiding personal results","loading":"Still
        |        loading...","mute":"Mute","noPreview":"Preview not available","sPers":"Show personal
        |        results","sPersD":"Currently showing personal
        |        results","unmute":"Unmute"},"nokjs":{"on":true},"time":{"hUnit":1500}},"skp":{"id":false,"l":"en-AU"},"tnv":{"t":false},"adsm":{},"amcl":{},"async":{},"bds":{},"ca":{},"dob":{},"erh":{},"hp":{},"hv":{},"lc":{},"lor":{},"ob":{},"r":{},"rsn":{},"sf":{},"sfa":{},"shb":{},"shlb":{},"srl":{},"st":{},"tbpr":{},"vs":{},"hsm":{},"j":{"ahipiou":true,"cspd":0,"hme":true,"icmt":false,"mcr":5,"tct":"
        |        \\u3000?"},"p":{"ae":true,"avgTtfc":2000,"brba":false,"dlen":24,"dper":3,"eae":true,"fbdc":500,"fbdu":-1,"fbh":true,"fd":1000000,"focus":true,"gpsj":true,"hiue":true,"hpt":310,"iavgTtfc":2000,"kn":true,"knrt":true,"maxCbt":1500,"mds":"clir,dfn,klg,prc,sp,sts,mbl_he,mbl_hs,mbl_re,mbl_rs,mbl_sv","msg":{"dym":"Did
        |        you mean:","gs":"Google Search","kntt":"Use the up and down arrow keys to select each result. Press Enter to go
        |        to the selection.","pcnt":"New Tab","sif":"Search instead for","srf":"Showing results
        |        for"},"nprr":1,"ophe":true,"pmt":250,"pq":true,"rpt":50,"sc":"psy-ab","sfcs":false,"tdur":50,"ufl":true},"pcc":{},"csi":{"acsi":true,"cbu":"/gen_204","csbu":"/gen_204"}};google.y.first.push(function(){google.loadAll(['gf','adp','adp','llc','an','skp','async','dob','vs']);if(google.med){google.med('init');google.initHistory();google.med('history');}google.History&&google.History.initialize('/');google.hs&&google.hs.init&&google.hs.init()});if(google.j&&google.j.en&&google.j.xi){window.setTimeout(google.j.xi,0);}
        |    </script>
        |</div>
        |<style id="_css0"></style>
        |<script>(function(){var b,c,d,e;function
        |    g(a,f){a.removeEventListener?(a.removeEventListener("load",f,!1),a.removeEventListener("error",f,!1)):(a.detachEvent("onload",f),a.detachEvent("onerror",f))}function
        |    h(a){e=(new Date).getTime();++c;a=a||window.event;a=a.target||a.srcElement;g(a,h)}var
        |    k=document.getElementsByTagName("img");b=k.length;
        |    for(var l=c=0,m;l<b;++l)m=k[l],m.complete||"string"!=typeof
        |    m.src||!m.src?++c:m.addEventListener?(m.addEventListener("load",h,!1),m.addEventListener("error",h,!1)):(m.attachEvent("onload",h),m.attachEvent("onerror",h));d=b-c;
        |    function n(){if(google.timers.load.t){google.timers.load.t.ol=(new
        |    Date).getTime();google.timers.load.t.iml=e;google.kCSI.imc=c;google.kCSI.imn=b;google.kCSI.imp=d;void
        |    0!==google.stt&&(google.kCSI.stt=google.stt);google.csiReport&&google.csiReport()}}window.addEventListener?window.addEventListener("load",n,!1):window.attachEvent&&window.attachEvent("onload",n);google.timers.load.t.prt=e=(new
        |    Date).getTime();})();
        |</script>
        |</body>
        |</html>
      """.stripMargin
}
