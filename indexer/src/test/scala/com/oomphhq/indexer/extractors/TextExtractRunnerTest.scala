package com.oomphhq.indexer.extractors

import com.oomphhq.indexer.extractors.TextExtractRunner._
import org.specs2.mutable.Specification

final class TextExtractRunnerTest extends Specification {
  "Absolute file names" should {
    "have their bundle relative paths computed" in {
      checkPath("/95-Americas/P1-1.pdf", "95-Americas/P1-1.pdf")
      checkPath("/290-Renaissance | Template/P1-1.pdf", "290-Renaissance | Template/P1-1.pdf")
      checkPath("/Shared/Layers/Template/P2-1.pdf", "Shared/Layers/Template/P2-1.pdf")
      checkPath("/Users/tom/Projects/mogeneration/servitron/woozle/Shared/Objects/TapGrey/S1-1.pdf", "Shared/Objects/TapGrey/S1-1.pdf")
      checkPath("/Users/tom/Projects/mogeneration/servitron/woozle/Shared/Layers/TapGrey/P1-1.pdf", "Shared/Layers/TapGrey/P1-1.pdf")
      checkPath("/Users/tom/Projects/mogeneration/servitron/woozle/990-About Us/VIPS2/S1-1.pdf", "990-About Us/VIPS2/S1-1.pdf")
      checkPath("/Users/tom/Projects/mogeneration/servitron/woozle/290-Renaissance | Template/P1-1.pdf", "290-Renaissance | Template/P1-1.pdf")
      checkPath("/Users/tom/Projects/mogeneration/servitron/woozle/800-Marriott Munich | Template/InvestmentHighlights/S1-1.html", "800-Marriott Munich | Template/InvestmentHighlights/S1-1.html")
      checkPath("/108-MR3134a-GHST S:S TOP | Menu-M-Run/L1-1.pdf", "108-MR3134a-GHST S:S TOP | Menu-M-Run/L1-1.pdf")
      checkPath("/110-MR3136b-X LITE SHORT | Menu-M-Run/L1-1.pdf", "110-MR3136b-X LITE SHORT | Menu-M-Run/L1-1.pdf")
      checkPath("/110-MR3136b-X LITE SHORT | Menu-M-Run/L1-1-FadeIn.pdf", "110-MR3136b-X LITE SHORT | Menu-M-Run/L1-1-FadeIn.pdf")
      checkPath("/2xu-s15-catalogue/245-WR3167b-PACE COMPRESSION SHORT | Menu-W-Run/L1-1.pdf", "245-WR3167b-PACE COMPRESSION SHORT | Menu-W-Run/L1-1.pdf")
    }
  }

  private def checkPath(filesystemPath: String, expectedPath: String) = {
    relativePathWithinBundle(filesystemPath) must beSome(expectedPath)
  }
}
