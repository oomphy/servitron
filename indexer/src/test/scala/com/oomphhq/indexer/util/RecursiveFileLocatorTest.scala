package com.oomphhq.indexer.util

import org.specs2.mutable.Specification
import com.oomphhq.common.util.test.TestResource

final class RecursiveFileLocatorTest extends Specification with TestResource {
  "A locator with a bogus filter" should {
    val locator = new RecursiveFileLocator(testResourceDirectory("/nested_folders/"))

    "find nothing" in {
      locator.locate(f => {false}) must be empty
    }
  }

  "Locating in a leaf directory" should {
    val locator = new RecursiveFileLocator(testResourceDirectory("/nested_folders/2/"))

    "finds all files down within the directory" in {
      locator.locate(f => {true}) must have size 1
    }
  }

  "Locating in a top-level directory" should {
    val locator = new RecursiveFileLocator(testResourceDirectory("/nested_folders/"))

    "finds all files underneath the base directory" in {
      locator.locate {f => f.name.endsWith(".png")} must have size 7
    }
  }
}
