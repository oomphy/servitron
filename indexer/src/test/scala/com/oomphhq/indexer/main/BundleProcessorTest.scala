package com.oomphhq.indexer.main

import java.io.InputStream

import com.oomphhq.common.util.Io._
import com.oomphhq.common.util.Unzipper
import com.oomphhq.common.util.test.TestResource
import com.oomphhq.indexer.extractors._
import com.oomphhq.indexer.extractors.pdf.PdfBoxExtractor
import com.oomphhq.indexer.test.NonLoggingSpecification

final class BundleProcessorTest extends NonLoggingSpecification with TestResource {
  "Processing a zip file" should {
    val extractor = new PdfBoxExtractor()

    "be able to pull the text out of all the PDFs within it" in {
      val resourceStream = testResourceStream("/zipped_content/dodgy_ad.zip")
      val zip = toFile(resourceStream)
      withTempDir { extractedBundle =>
        withTempFile { index =>
          Unzipper.unzip(zip, extractedBundle)
          DirectoryExtractor.extract(extractedBundle) { (indexable: Indexable) =>
            indexable.path must contain("10-Contents/L1-1.pdf")
            indexable.pageNumber must beEqualTo(1)
            indexable.content must contain("X LITE SHORT")
            indexable.content must contain("Short is light weight, breathable and moves freely")
            indexable.content must contain("Super light weight short")
            indexable.content must contain("FABRICS")
            indexable.content must contain("COLOURS")
          }
        }
      }
    }
  }
}
