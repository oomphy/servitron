package com.oomphhq.indexer.test

import ch.qos.logback.classic
import ch.qos.logback.classic._
import org.slf4j.{Logger, LoggerFactory}

// Turns off logging from library code so that it doesn't pollute the console when running specs.
trait NonLoggingSpecification extends org.specs2.mutable.Specification {
  try {
    val lc = LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext]
    val rootLogger: classic.Logger = lc.getLogger(Logger.ROOT_LOGGER_NAME)
    rootLogger.setLevel(Level.OFF)
  } catch {
    case _: Throwable => println("Couldn't configure logging")
  }
}
