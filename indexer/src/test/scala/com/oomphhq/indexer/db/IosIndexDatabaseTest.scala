package com.oomphhq.indexer.db

import com.oomphhq.common.util.Io
import com.oomphhq.common.util.Io._
import com.oomphhq.common.util.io.File
import com.oomphhq.common.util.test.TestResource
import com.oomphhq.indexer.index.db.IosIndexDatabase._
import org.specs2.mutable.Specification

final class IosIndexDatabaseTest extends Specification with TestResource {
  val extractedContentPath = "/extracted_content/financialstandard.feb2013/10-Cover/L1-2-MoveInFromLeft.pdf-Page-0001.txt"

  "A new database" should {
    "be createable" in {
      withTempFile { dbFile =>
        newDatabase(dbFile).create
        dbFile.file.length() must beGreaterThan(0L)
      }
    }

    "can have data written to it" in {
      val extractedContentPath = "/extracted_content/financialstandard.feb2013/10-Cover/L1-4-FadeIn.pdf-Page-0001.txt"
      withTempFile { dbFile =>
        val db = newDatabase(dbFile).create
        db.addExtraction(extractedContentPath, 1, slurp(testResourceStream(extractedContentPath)))
        dbFile.file.length() must beGreaterThan(0L)
      }
    }

    "can have a full text extraction written to it & read back out" in {
      val extractedContentPath = "/extracted_content/lots_of_text.txt"
      withTempFile { dbFile =>
        val db = newDatabase(dbFile).create
        val content = slurp(testResourceStream(extractedContentPath))
        db.addExtraction(extractedContentPath, 1, content)
        db.rows must beEqualTo(1)
        db.map { (path, page, content) =>
          path must beEqualTo(extractedContentPath)
          page must beEqualTo(1)
          content must beEqualTo(content)
        }
        Io.copy(dbFile, File.file("/tmp/tmp.sqlite"))
      }
    }
  }

  "An existing database" should {
    "can be opened & read" in {
      Io.withTempFile { tmp =>
        Io.copy(toFile(testResourceStream("/extracted_content/extracted_text.sqlite")), tmp)
        val db = openDatabase(tmp)
        db.rows must beEqualTo(1)
      }
    }

    "can be opened & written to" in {
      Io.withTempFile { tmp =>
        Io.copy(toFile(testResourceStream("/extracted_content/extracted_text.sqlite")), tmp)
        val in = testResourceStream(extractedContentPath)
        openDatabase(tmp).addExtraction(extractedContentPath, 1, slurp(in))
      }
    }
  }
}
