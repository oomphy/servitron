package com.oomphhq.indexer.resources

import javax.ws.rs._
import javax.ws.rs.core.MediaType
import com.yammer.metrics.annotation.Timed
import com.oomph.clients.AvailableBundle
import com.oomphhq.indexer.Indexer
import com.oomph.s3.S3Key

@Path("/index/add")
@Produces(Array(MediaType.APPLICATION_JSON))
class IndexingResource(indexer: Indexer) {
  type R[T] = Map[String, T]
  def point[A](a: A): R[A] = Map("result" -> a)

  @POST
  @Timed
  def indexIssueVersion(@QueryParam("itunes_product_id") @DefaultValue("") itunesProductId: String,
                         @QueryParam("s3_key") @DefaultValue("") s3Key: String): R[Boolean] = {
    val bundle = AvailableBundle(itunesProductId, S3Key(s3Key))
    indexer.indexAndPatch(bundle)
    point(true)
  }
}

@Path("/content_map")
@Produces(Array(MediaType.APPLICATION_JSON))
class ContentMapResource {
  type R[T] = Map[String, T]
  def point[A](a: A): R[A] = Map("result" -> a)
}
