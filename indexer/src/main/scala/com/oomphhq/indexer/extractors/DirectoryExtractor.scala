package com.oomphhq.indexer.extractors

import com.oomphhq.indexer.util.RecursiveFileLocator
import com.oomphhq.common.util.io._
import com.oomphhq.indexer.extractors.pdf.PdfBoxExtractor
import com.oomphhq.indexer.extractors.text.PlainTextTextExtractor
import com.oomphhq.indexer.extractors.html.HtmlTextExtractor

object DirectoryExtractor {
  val extractors = Map(
    "pdf" -> new PdfBoxExtractor(),
    "txt" -> new PlainTextTextExtractor(),
    "text" -> new PlainTextTextExtractor(),
    "html" -> new HtmlTextExtractor(),
    "htm" -> new HtmlTextExtractor()
  )

  def extract(baseDir: Directory)(f: Indexable => Unit) {
    val locator = new RecursiveFileLocator(baseDir)
    val files = locator.locate(file => extractors.keys.exists(ext => file.name.endsWith(ext)))
    files.foreach { file =>
      file.extension.flatMap(extractors.get).foreach { e =>
        file.in { in =>
          new TextExtractRunner(e).extract(file.path.replace(baseDir.path, ""), in) { indexable =>
            f(indexable)
          }
        }
      }
    }
  }
}
