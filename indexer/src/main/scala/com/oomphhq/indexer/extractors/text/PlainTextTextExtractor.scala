package com.oomphhq.indexer.extractors.text

import com.oomphhq.indexer.extractors.TextExtractor
import java.io.InputStream
import com.oomphhq.common.util.Io

final class PlainTextTextExtractor extends TextExtractor {
  def extract(fileName: String, in: InputStream) = Seq(Io.slurp(in))
}
