package com.oomphhq.indexer.extractors.pdf

import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.util.PDFTextStripper
import java.io.{StringWriter, InputStream}
import com.oomphhq.common.util.Io
import com.yammer.dropwizard.logging.Log
import com.oomphhq.indexer.extractors.TextExtractor
import scala.collection.mutable.ListBuffer

final class PdfBoxExtractor extends TextExtractor {
  val log = Log.forThisClass()

  def extract(fileName: String, in: InputStream) = {
    val extracted = new ListBuffer[String]
    try {
      extracted ++= meh(in)
    } catch {
      case e: Throwable => log.warn("Error processing %s, ignored: %s".format(fileName, e))
    }
    extracted.toSeq
  }

  private def meh(in: InputStream): ListBuffer[String] = {
    val extracted = new ListBuffer[String]
    val document = PDDocument.load(in)
    try {
      for (i <- 1 to document.getDocumentCatalog.getPages.getCount.asInstanceOf[Int]) {
        val stripper = new PDFTextStripper()
        stripper.setStartPage(i)
        stripper.setEndPage(i)
        val writer = new StringWriter()
        stripper.writeText(document, writer)
        extracted += writer.toString
      }
    } finally {
      Io.close(document)
    }
    extracted
  }
}
