package com.oomphhq.indexer.extractors

import java.io.InputStream
import java.util.regex._

import scalaz.\/._

case class Indexable(path: String, pageNumber: Int, content: String)

object TextExtractRunner {
  val supportedTypes = "(%s)".format(DirectoryExtractor.extractors.keys.mkString("|"))
  val normalPdfPath = "\\d+-[^/]+/.*\\.%s".format(supportedTypes)
  val sharedLayerPath = "Shared/Layers/[^/]+/.*\\.%s".format(supportedTypes)
  val sharedObjectPath = "Shared/Objects/[^/]+/.*\\.%s".format(supportedTypes)
  lazy val textExtractionPattern = Pattern.compile("^.*/(%s|%s|%s)$".format(normalPdfPath, sharedLayerPath, sharedObjectPath))

  def relativePathWithinBundle(fileName: String): Option[String] = fromTryCatch {
    val matcher = textExtractionPattern.matcher(fileName)
    matcher.matches()
    matcher.group(1)
  }.toOption
}

final class TextExtractRunner(extractor: TextExtractor) {

  import com.oomphhq.indexer.extractors.TextExtractRunner._

  def extract(fileName: String, source: InputStream)(f: Indexable => Unit) {
    val pages = extractor.extract(fileName, source)
    pages.zipWithIndex foreach {
      case (p, i) => {
        relativePathWithinBundle(fileName).foreach { s => f(Indexable(s, i + 1, p))}
      }
    }
  }
}
