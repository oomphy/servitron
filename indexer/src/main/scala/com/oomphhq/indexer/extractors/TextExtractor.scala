package com.oomphhq.indexer.extractors

import java.io.InputStream

trait TextExtractor {
  def extract(fileName: String, in: InputStream): Seq[String]
}
