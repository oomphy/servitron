package com.oomphhq.indexer.extractors.html

import com.oomphhq.indexer.extractors.TextExtractor
import java.io.InputStream
import net.htmlparser.jericho._

// Other options:
// * http://jsoup.org/cookbook/extracting-data/attributes-text-html
// * http://htmlparser.sourceforge.net
final class HtmlTextExtractor extends TextExtractor {
  def extract(fileName: String, in: InputStream) = {
    val source = new Source(in)
    source.fullSequentialParse()
    val content = source.getTextExtractor.setIncludeAttributes(false).toString
    Seq(content)
  }
}
