package com.oomphhq.indexer.util

import com.oomphhq.common.util.io.{Directory, File}
import collection.mutable.ListBuffer

final class RecursiveFileLocator(baseDir: Directory) {
  def locate(filter: File => Boolean): List[File] = {
    // TODO Remove the mutability
    val extracted = new ListBuffer[File]
    locateit(baseDir, filter)
    def locateit(base: Directory, ff: File => Boolean) {
      for (dir <- base.listDirectories()) {
        locateit(dir, ff)
      }
      extracted ++= base.listFiles(ff)
    }
    extracted.toList
  }
}

