package com.oomphhq.indexer.configuration

import com.oomph.configuration.ApiConfiguration
import org.codehaus.jackson.annotate.{JsonIgnoreProperties, JsonProperty}
import javax.validation.Valid
import javax.validation.constraints.NotNull
import com.yammer.dropwizard.config.Configuration
import com.yammer.dropwizard.client.HttpClientConfiguration
import com.oomph.s3.S3Configuration

@JsonIgnoreProperties(ignoreUnknown = true)
class IndexerConfiguration extends Configuration {
  @Valid @NotNull @JsonProperty
  val dashboard: ApiConfiguration = null

  @Valid @NotNull @JsonProperty
  val sachin: ApiConfiguration = null

  @Valid @NotNull @JsonProperty
  val s3: S3Configuration = null

  @Valid @NotNull @JsonProperty
  val httpClient = new HttpClientConfiguration
}
