package com.oomphhq.indexer

import com.oomphhq.common.util.{Io, ZipAddFile, Zipper}
import java.util.{Date, UUID}
import com.oomph.clients.{AvailableBundle, IssueVersionCompact, Dashboard}
import main.BundleProcessor
import scalaz.std.list._
import scalaz.syntax.foldable._
import scalaz.Order
import dispatch.Http
import com.oomph.s3.{FileStore, S3Key}

final class Indexer(http: Http, s3: FileStore[S3Key], dashboard: Dashboard) {
  def indexAll(itps: List[String]) {
    itps.foreach { (itp: String) =>
      latestContent(dashboard, itp).foreach(indexAndPatch)
    }
  }

  def latestContent(d: Dashboard, itp: String): Option[AvailableBundle] = {
    val versions: List[IssueVersionCompact] = http(d.issueVersionsForIssue(itp))
    versions.maximum(implicitly[Order[Date]].contramap(_.updatedAt)).map(s => AvailableBundle(itp, s.s3Key))
  }

  def indexAndPatch(originalBundle: AvailableBundle) {
    println(originalBundle)
    BundleProcessor.indexBundle(s3, originalBundle.contentKey)(indexFile => {
      Io.withTempFile { newZip =>
        newZip.out()(Zipper.zip(_, ZipAddFile(indexFile, Some("OomphTextIndex.sqlite"))))
        val key: S3Key = generateKey
        val md5: String = s3.upload(newZip, key)
        println(" * Patching dashboard (%s)" format key)
        val result: (Boolean, String) = http(dashboard.patch(originalBundle.itunesProductId, originalBundle.contentKey, key, md5))
        println(result)
      }
    })
  }

  def generateKey: S3Key = {
    val uuid: String = UUID.randomUUID.toString
    S3Key("indexer_path/%s/patch.zip" format (uuid))
  }
}
