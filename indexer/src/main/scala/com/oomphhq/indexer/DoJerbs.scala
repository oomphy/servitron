package com.oomphhq.indexer

import com.oomph.clients.{BundleJerb, Sachin}
import dispatch.{url, Http}
import scalaz.\/
import com.yammer.dropwizard.logging.Log

object DoJerbs {
  val log = Log.forThisClass()
  def url_(s: String) = url(s).as_!("what", "foo")

  def apply(http: Http, sachin: Sachin, indexer: Indexer) {
    \/.fromTryCatch(apply2(http, sachin, indexer)).fold(
      l = e => log.error(e, e.getMessage),
      r = _ => ()
    )
  }

  def apply2(http: Http, sachin: Sachin, indexer: Indexer) {
    val jerbs: List[BundleJerb] = http(sachin.indexingQueue)
    log.info("Starting the lot: %s" format jerbs)
    jerbs.foreach { bundleJerb =>
      log.info("%s" format bundleJerb)
      http(url_(bundleJerb.jerb.startUrl).POST >|)
      log.info("Started.")
      \/.fromTryCatch(indexer.indexAndPatch(bundleJerb.bundle)).fold(
        r = _ => {
          log.info("success")
          http(url_(bundleJerb.jerb.successUrl).POST >|)
        },
        l = e => {
          log.info("failure: %s" format e)
          http(url_(bundleJerb.jerb.failureUrl).POST >|)
        }
      )
    }
  }
}
