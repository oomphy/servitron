package com.oomphhq.indexer

import dispatch.Http
import main.BundleProcessor
import org.apache.http.client.HttpClient
import com.yammer.dropwizard.client.HttpClientFactory
import com.yammer.dropwizard.{AbstractService, ScalaService}
import com.yammer.dropwizard.config.Environment
import com.yammer.dropwizard.cli.{Command, ConfiguredCommand}
import org.apache.commons.cli.CommandLine
import com.oomph.clients.{Sachin, Dashboard}
import com.oomphhq.indexer.resources.IndexingResource
import com.oomphhq.indexer.configuration.IndexerConfiguration
import com.oomph.configuration.Request
import java.util.concurrent.TimeUnit
import com.oomph.dropwizard.EnvironmentUtils
import com.oomph.s3.S3Client
import com.oomphhq.common.util.io.File

class IndexerService extends ScalaService[IndexerConfiguration]("indexer") {
  addCommand(cliRunner)
  addCommand(indexLocal)

  def initialize(configuration: IndexerConfiguration, environment: Environment) {
    val http = EnvironmentUtils.managedHttp(environment, configuration.httpClient)
    val s3 = S3Client(configuration.s3)
    val dashboard = Dashboard(Request.fromConfiguration(configuration.dashboard))
    val sachin = Sachin(Request.fromConfiguration(configuration.sachin))
    val indexer: Indexer = new Indexer(http, s3, dashboard)

    val singleThreader = environment.managedScheduledExecutorService("jerber-%d", 1)
    // Every minute, with a minute delay between runs
    val jerber = new Runnable {
      def run() {
        DoJerbs(http, sachin, indexer)
      }
    }
    singleThreader.scheduleWithFixedDelay(jerber, 1, 1, TimeUnit.MINUTES)

    environment.addResource(new IndexingResource(indexer))
  }

  def cliRunner: ConfiguredCommand[IndexerConfiguration] = {
    new ConfiguredCommand[IndexerConfiguration]("index-issue", "run the indexer on some issues") {
      def run(service: AbstractService[IndexerConfiguration], configuration: IndexerConfiguration, params: CommandLine) {
        val factory = new HttpClientFactory(configuration.httpClient)
        val http = new Http {
          override def make_client: HttpClient = factory.build()
        }
        val itps = params.getArgs.toList
        println("TODO: %s" format itps.reduce(_ + ", " + _))

        val indexer = new Indexer(http, S3Client(configuration.s3), Dashboard(Request.fromConfiguration(configuration.dashboard)))
        indexer.indexAll(itps)
        http.shutdown()
      }
    }
  }

  def indexLocal: Command = {
    new Command("index-local", "run the indexer on a local zip file") {
      def run(service: AbstractService[_], params: CommandLine) {
        val file = params.getArgs.toList.head
        BundleProcessor.indexBundle2(File.file(file)) { f =>
          val newFile = f.move(File.file(new java.io.File("index.sqlite")))
          //file.jFile.renameTo(newFile)
          println("Index created at: %s".format(newFile.path))
        }
      }
    }
  }
}
