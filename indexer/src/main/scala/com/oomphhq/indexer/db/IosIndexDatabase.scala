package com.oomphhq.indexer.index.db

import com.oomphhq.common.util.io.File
import org.scalaquery.ql.extended.{ExtendedTable => Table}
import org.scalaquery.session._
import org.sqlite.JDBC
import org.scalaquery.ql.extended.SQLiteDriver.Implicit._
import org.scalaquery.session.Database.threadLocalSession
import org.scalaquery.ql.Query
import collection.immutable.IndexedSeq

object ExtractedText extends Table[(Int, String, Int, String)]("EXTRACTED_TEXT") {
  def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)

  def path = column[String]("PATH")

  def page = column[Int]("PAGE")

  def content = column[String]("CONTENT")

  def * = id ~ path ~ page ~ content
}

object IosIndexDatabase {
  def newDatabase(dbFile: File): NewIosIndexDatabase = new NewIosIndexDatabase {val databaseFile = dbFile}

  def openDatabase(databaseFile: File): OpenIosIndexDatabase = {
    val db_ = Database.forURL(JDBC.PREFIX + databaseFile.path, driver = classOf[JDBC].getName)
    new OpenIosIndexDatabase { val db = db_}
  }
}

sealed trait NewIosIndexDatabase {
  val databaseFile: File

  def create: OpenIosIndexDatabase = {
    val db_ = Database.forURL(JDBC.PREFIX + databaseFile.path, driver = classOf[JDBC].getName)
    db_.withSession {
      ExtractedText.ddl.create
    }
    new OpenIosIndexDatabase { val db = db_}
  }
}

sealed trait OpenIosIndexDatabase {
  val db: Database

  def addExtraction(path: String, page: Int, content: String) {
    db.withSession {
      (ExtractedText.path ~ ExtractedText.page ~ ExtractedText.content).insertAll((path, page, content))
    }
  }

  def rows: Int = db.withSession {
    Query(ExtractedText.count).first
  }

  // TODO Why do we need to call build here? It only forces the execution if it's used.
  def map[T](f: (String, Int, String) => T): IndexedSeq[T] = db.withSession {
    Query(ExtractedText.path ~ ExtractedText.page ~ ExtractedText.content).mapResult {case (path, page, content) => f(path, page, content)}.build()
  }
}
