package com.oomphhq.indexer.main

import com.oomphhq.common.util.{Io, Unzipper}, Io._
import com.oomphhq.common.util.io.File
import com.oomph.s3.FileStore

import com.oomphhq.indexer.extractors.DirectoryExtractor
import com.oomphhq.indexer.index.db.IosIndexDatabase._

object BundleProcessor {
  def indexBundle[K](s3: FileStore[K], key: K)(f: File => Unit) {
    s3.analyzeRemoteFile(key)(indexBundle2(_)(f))
  }

  def indexBundle2(downloadedBundle: File)(f: File => Unit) {
    withTempDir { extractedBundle =>
      withTempFile { index =>
        Unzipper.unzip(downloadedBundle, extractedBundle)
        val db = newDatabase(index).create
        DirectoryExtractor.extract(extractedBundle) { indexable =>
          db.addExtraction(indexable.path, indexable.pageNumber, indexable.content)
        }
        f(index)
      }
    }
  }
}
