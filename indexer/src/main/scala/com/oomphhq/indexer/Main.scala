package com.oomphhq.indexer

object Main {
  def main(args: Array[String]) {
    new IndexerService().run(args)
  }
}
