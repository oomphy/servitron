organization := "com.oomphhq"

name := "indexer"

version := "1.0"

scalaVersion := "2.9.2"

resolvers += "snowtide-releases" at "http://maven.snowtide.com/releases"

resolvers += "Scala-Tools Maven2 Repository" at "http://scala-tools.org/repo-releases"

libraryDependencies ++= Seq(
  "org.scalaz" %% "scalaz-core" % "7.0-SNAPSHOT",
  "com.yammer.dropwizard" % "dropwizard-core" % "0.5.1",
  "com.yammer.dropwizard" % "dropwizard-scala_2.9.1" % "0.5.1",
  "org.xerial" % "sqlite-jdbc" % "3.7.2",
  "org.scalaquery" % "scalaquery_2.9.1-1" % "0.10.0-M1",
  "net.databinder" %% "dispatch-http" % "0.8.8",
  "org.apache.pdfbox" % "pdfbox" % "1.8.5",
  "org.apache.lucene" % "lucene-core" % "3.5.0",
  "com.amazonaws" % "aws-java-sdk" % "1.3.20",
  "net.htmlparser.jericho" % "jericho-html" % "3.3",
  "org.specs2" %% "specs2" % "1.12.3" % "test"
)

resolvers += "Codahale's Repo" at "http://repo.codahale.com" // Dropwizard

exportJars := true

//seq(com.github.retronym.SbtOneJar.oneJarSettings: _*)

resolvers += "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/" // Argonaut and scalaz

StageKeys.stageMainAndArgs := "com.oomphhq.indexer.Main server indexer.yml"

// "nohup /usr/bin/java -jar /opt/indexer/indexer.jar server /opt/indexer/indexer.yml > /opt/indexer/log.log 2>&1 &", ""

Stage.defaultSettings
