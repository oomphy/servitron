class CreateContentMaps < ActiveRecord::Migration
  def change
    create_table :content_maps, :primary_key => :s3_key do |t|
      t.string :s3_key, :null => false
      t.binary :content_map, :null => false
    end
    change_column :content_maps, :s3_key, :string
  end
end

