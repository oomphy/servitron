class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.string :s3_key, :null => false
      t.column :sc_bytes, :bigint, :null => false
      t.datetime :timestamp, :null => false
    end
  end
end
