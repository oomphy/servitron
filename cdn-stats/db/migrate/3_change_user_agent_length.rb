class ChangeUserAgentLength < ActiveRecord::Migration
  def change
    change_column :raw_logs, :c_user_agent, "VARCHAR(255)", :null => false
  end
end

