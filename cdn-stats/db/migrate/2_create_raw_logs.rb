class CreateRawLogs < ActiveRecord::Migration
  def change
    create_table :raw_logs do |t|
      t.datetime :timestamp, :null => false
      t.string :time_taken, :null => false
      t.string :c_ip, :null => false
      t.string :filesize, :null => false
      t.string :s_ip, :null => false
      t.string :s_port, :null => false
      t.string :sc_status, :null => false
      t.string :sc_bytes, :null => false
      t.string :cs_method, :null => false
      t.string :cs_uri_stem, :null => false
      t.string :rs_duration, :null => false
      t.string :rs_bytes, :null => false
      t.string :c_referrer, :null => false
      t.string :c_user_agent, :null => false
      t.string :customer_id, :null => false
      t.string :x_ec_custom_1, :null => false
    end
  end
end
