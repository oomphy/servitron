class AddIndices < ActiveRecord::Migration
  def change
    change_column :raw_logs, :c_user_agent, "VARCHAR(255)", :null => false
    add_index :logs, :s3_key
    add_index :logs, :timestamp
  end
end

