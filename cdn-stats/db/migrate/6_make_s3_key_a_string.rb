class MakeS3KeyAString < ActiveRecord::Migration
    def change
        change_column :issue_versions, :s3_key, :string
    end
end