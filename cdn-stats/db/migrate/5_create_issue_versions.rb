class CreateIssueVersions < ActiveRecord::Migration
  def change
    create_table :issue_versions, :primary_key => :s3_key do |t|
      t.string :s3_key, :null => false
      t.string :application_bundle_id, :null => false
      t.string :itunes_product_id, :null => false
    end
  end
end

