require 'json'

puts JSON.parse($stdin.read).fetch("result").map { |x| "#{x["day"]},#{(x["count"].to_i / 1000.0 / 1000.0).to_i}"}.join("\n")
