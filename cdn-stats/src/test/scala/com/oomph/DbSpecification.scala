package com.oomph

import db.tables.{Logs, IssueVersions}
import db.LogsAggregator
import org.specs2.mutable._

import org.scalaquery.session.{Session, Database}
import org.scalaquery.ql.DDL
import java.util.concurrent.{TimeUnit, Executors}

class ExecutorReschedulingSpec extends Specification {
//  "rescheduling" should {
//    "reschedule after failure" in {
//      var count = 0
//      val executor = Executors.newScheduledThreadPool(1)
//      ExecutorUtils.restartScheduleOnError(executor.scheduleWithFixedDelay(new Runnable {
//        def run() {
//          count += 1
//          throw new RuntimeException("okay")
//        }
//      }, 1, 1, TimeUnit.SECONDS))
//      Thread.sleep(3000)
//      count must be_>=(2)
//    }
//  }
}

class DbSpecification extends Specification with Before {
  lazy val db = Database.forURL("jdbc:mysql://localhost:3306/cdn_stats_test", user = "root", password = "")

  import org.scalaquery.ql.extended.MySQLDriver.Implicit._

  def before {
    db.withSession[Unit]((s: Session) => {
      implicit val se = s
      val ddl: DDL = Logs.ddl ++ IssueVersions.ddl
      println(ddl.createStatements.mkString("\n"))
      ddl.drop
      ddl.create
      (Logs.s3Key ~ Logs.scBytes ~ Logs.timestamp).insert("a", 1 : Long, new java.sql.Timestamp(new java.util.Date().getTime - 50000))
      (Logs.s3Key ~ Logs.scBytes ~ Logs.timestamp).insert("a", 1 : Long, new java.sql.Timestamp(new java.util.Date().getTime - 50000))
      (Logs.s3Key ~ Logs.scBytes ~ Logs.timestamp).insert("a", 1 : Long, new java.sql.Timestamp(new java.util.Date().getTime - 50000))
      ()
    })

  }

  "foo" should {
    "x" in {
      db.withSession[Unit]{ (s: Session) =>
        LogsAggregator.flatten(100)(s)
        println((for {l <- Logs} yield l).list()(s))
        println("***")
        LogsAggregator.flatten(100)(s)
        println((for {l <- Logs} yield l).list()(s))
      }
      5.must_==(3)
    }
  }
}
