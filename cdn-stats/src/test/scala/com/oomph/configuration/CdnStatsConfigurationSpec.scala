package com.oomph.configuration

import org.specs2.mutable._
import com.yammer.dropwizard.validation.Validator
import com.yammer.dropwizard.config.ConfigurationFactory
import java.io.File

class CdnStatsConfigurationSpec extends Specification {
  "loading configuration" should {
    "work" in {
      val factory = ConfigurationFactory.forClass(classOf[CdnStatsConfiguration], new Validator())
      val r: CdnStatsConfiguration = factory.build(new File("cdn-stats/cdn-stats.yml"))
      5 must_== 5
    }
  }
}