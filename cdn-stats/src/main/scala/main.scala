import com.oomph.CdnStatsService

object Main {
  def main(args: Array[String]) = { 
    new CdnStatsService().run(args)
  }
}
