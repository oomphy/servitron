package com.oomph

import clients.Dashboard
import configuration.{ApiConfiguration, Request}
import s3.S3Key
import dispatch.Http

object NicksAnalyzer {
  def run {
    val http = new Http
    val dashboard = Dashboard(Request.fromConfiguration(new ApiConfiguration {
      override val uriBase: String = "https://dashboard.oomphhq.com/publish/api/v1"
      override val user: String = "what"
      override val password: String = "foo"
    }))
    val creds = Credentials("ftp.lax.5480.edgecastcdn.net", "cdnstats@oomphhq.com", "publish this mofo")
    val sourceDirectories = List("/Users/nkpart/INC0055341/Nov", "/Users/nkpart/INC0055341/Dec")


    val issues: List[String] = List("au.com.acpmagazines.gourmettraveller.2012.october", "au.com.acpmagazines.gourmettraveller.2012.november", "au.com.acpmagazines.gourmettraveller.2012.december")

    go(creds, "oompf.mogeneration.com", dashboard, http, sourceDirectories, issues)
  }

  def go(creds: Credentials, origin: String, dashboard: Dashboard, http: Http, sourceDirs: List[String], issues: List[String]) {
    val s3Keys: List[(String, List[S3Key])] = issues.map(v => (v, http(dashboard.issueVersionsForIssue(v)).map(_.s3Key)))
    val keysToIssues: Map[S3Key, String] = s3Keys.flatMap(v => v._2.map(x => (x, v._1))).toMap
    sourceDirs.foreach { sourceDir =>
      val m = collection.mutable.Map[String, Long]()
      FileUtils.filesIn(sourceDir).toList.foreach { f =>
        println(f)
        CdnIngestor.readRawLogs(origin, f).toOption.get.foreach { g: S3ObjectDownload =>
          keysToIssues.get(S3Key(g.s3_key)).foreach { k =>
            m.update(k, m.getOrElseUpdate(k, 0) + g.sc_bytes)
          }
        }
      }
      println(m)
    }
  }
}
