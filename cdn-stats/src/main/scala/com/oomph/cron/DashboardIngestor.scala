package com.oomph.cron

import com.oomph.clients.Dashboard
import org.joda.time._
import Duration.standardHours
import com.yammer.dropwizard.logging.Log
import java.util.Date
import dispatch.Http
import org.scalaquery.session.{Session, Database}

class DashboardIngestor(http: Http, dashboard: Dashboard, db: Database) {
  val log = Log.forClass(getClass)

  def ingestAfter(after: Date) {
    log.info("Getting issue versions after " + after)
    val xs = http(dashboard.newIssueVersions(after))
    log.info("%s new issue versions" format (xs))
    db.withSession { (s: Session) =>
      s.withPreparedStatement("REPLACE INTO issue_versions (application_bundle_id, itunes_product_id, s3_key) values (?, ?, ?)")(stmt => {
        xs foreach { log =>
          stmt.setString(1, log.application_bundle_id)
          stmt.setString(2, log.itunes_product_id)
          stmt.setString(3, log.s3_key)
          stmt.execute()
        }
      })
    }
  }

  def recentlyR: Runnable = new Runnable() {
    def run() {
      val now: DateTime = new DateTime()
      ingestAfter(now.minus(standardHours(1)).toDate)
    }
  }
}

