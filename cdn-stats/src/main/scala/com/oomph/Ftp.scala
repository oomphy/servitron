package com.oomph

import it.sauronsoftware.ftp4j.{FTPClient, FTPFile}
import com.yammer.dropwizard.logging.{Log => WizardLog}

case class Credentials(host: String, user: String, pass: String)

sealed trait FtpFile {
  val filename: String
  def download(dest: java.io.File)
  def delete
}

object FtpFile {
  def apply(client: FTPClient, jf: FTPFile): FtpFile = new FtpFile {
    val filename = jf.getName
    def download(dest: java.io.File) = client.download(jf.getName, dest)
    def delete = client.deleteFile(jf.getName)
    override def toString = jf.toString
  }
}

object Ftp {
  val log = WizardLog.forClass(getClass)

  def withFtp[A](creds: Credentials)(f: FTPClient => A): A = {
    val c = {
      val ftp = new FTPClient
      ftp.connect(creds.host)
      ftp.login(creds.user, creds.pass)
      ftp
    }
    val a = f(c)
    c.disconnect(true)
    a
  }

  def enumerateFiles(creds: Credentials, dir: String)(f: FtpFile => Unit) {
    withFtp(creds) { client =>
      client.changeDirectory(dir)
      client.list().foreach { ftpFile =>
        if (ftpFile.getType == FTPFile.TYPE_FILE) {
          f(FtpFile(client, ftpFile))
        }
      }
    }
  }
}
