package com.oomph.configuration

import org.hibernate.validator.constraints.NotEmpty
import org.codehaus.jackson.annotate.JsonProperty

class FtpConfiguration {
  @NotEmpty @JsonProperty
  val host: String = null

  @NotEmpty @JsonProperty
  val user: String = null

  @NotEmpty @JsonProperty
  val password: String = null

  @NotEmpty @JsonProperty
  val logDir: String = null
}
