package com.oomph.configuration

import com.yammer.dropwizard.config.Configuration
import org.codehaus.jackson.annotate._
import org.hibernate.validator.constraints.NotEmpty
import javax.validation.Valid
import javax.validation.constraints.NotNull
import com.yammer.dropwizard.client.HttpClientConfiguration
import com.oomph.Credentials

class CdnStatsConfiguration extends Configuration {
  @Valid @NotNull @JsonProperty
  val dashboard: ApiConfiguration = null

  @Valid @NotNull @JsonProperty
  val ftp: FtpConfiguration = new FtpConfiguration

  @NotEmpty @JsonProperty
  val localLogDir: String = null

  @NotEmpty @JsonProperty
  val databaseYml: String = null

  @NotEmpty @JsonProperty
  val database: String = null

  @Valid @NotNull @JsonProperty
  val httpClient = new HttpClientConfiguration

  @Valid @NotNull @JsonProperty
  val railsEnv: String = "production"

  @Valid @NotNull
  val cdnRoot: String = "oompf.mogeneration.com"

  @Valid @JsonProperty
  val logIngestionEnabled: Boolean = true

  @Valid @JsonProperty
  val logAggregationEnabled: Boolean = true

  def credentials: Credentials = Credentials(ftp.host, ftp.user, ftp.password)
}
