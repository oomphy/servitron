package com.oomph.db

import org.scalaquery.session.Session
import java.sql.{ResultSet, Statement, Timestamp}
import com.yammer.dropwizard.logging.Log
import tables.Logs

object LogsAggregator {
  val logger = Log.forThisClass()
  import org.scalaquery.ql.extended.MySQLDriver.Implicit._
  // Keyed by S3Key and Daily timestamp, values are record ids that contribute to the total, and the total
  type Store = collection.mutable.Map[(String, Timestamp), (List[Int], Long)]

  def flatten[D](limit: Int)(implicit s: Session) {
    val store = buildStore(s, limit)
    s.withTransaction {
      ingestStore(store)
    }
  }

  def buildStore[D](s: Session, limit: Int): LogsAggregator.Store = withStatement(s) { st =>
    enableStreaming(st)
    val sql: String = queryUnrolledLogs(limit)
    executeResultSet(st, sql){ rs =>
      val store: Store = collection.mutable.Map.empty
      while (rs.next()) {
        try {
          val logStamp = rs.getTimestamp(4)
          val startOfDayStamp = startDay(logStamp)
          // Skip records that are already accumulators
          if (startOfDayStamp != logStamp) {
            val (logId, logKey, logBytes) = (rs.getInt(1), rs.getString(2), rs.getLong(3))
            if (logId % 100000 == 0) {
              logger.info("Progress: %d" format logId)
            }
            val storeKey = (logKey, startOfDayStamp)
            val (ids, c) = store.getOrElse(storeKey, (Nil, 0L))
            val newB = (logId :: ids, c + logBytes)
            store.put(storeKey, newB)
          }
        } catch {
          // some timestamp values are bad
          case e => logger.error(e.getMessage)
        }
      }
      store
    }
  }

  def queryUnrolledLogs[D](limit: Int): String = {
    Logs.where(v => QueryLogs.TIME(v.timestamp) =!= "00:00:00").map(otherLog =>
      otherLog.id ~ otherLog.s3Key ~ otherLog.scBytes ~ otherLog.timestamp
    ).take(limit).selectStatement
  }

  def enableStreaming[D](st: Statement) {
    st.setFetchSize(Integer.MIN_VALUE)
  }

  def withStatement[A](s: Session)(f: Statement => A): A = {
    val st = s.createStatement()
    val r = f(st)
    st.close()
    r
  }

  def executeResultSet[A](st: Statement, sql: String)(f: ResultSet => A): A = {
    st.execute(sql)
    val rs = st.getResultSet
    val r = f(rs)
    if (!rs.isClosed) {
      rs.close()
    }
    r
  }

  def ingestStore(s: Store)(implicit session: Session) {
    logger.info("%d things in the store" format s.size)

    session.withPreparedStatement("UPDATE logs SET sc_bytes = sc_bytes + ? WHERE id = ?") { updateStatement =>
    session.withPreparedStatement("SELECT id FROM logs where timestamp = ? AND s3_key = ?") { dailyLogFinderStatement => {
        def findDaily(t: Timestamp, k: String): ResultSet = {
          dailyLogFinderStatement.setTimestamp(1, t)
          dailyLogFinderStatement.setString(2, k)
          dailyLogFinderStatement.executeQuery()
        }

        var count = 0
        var newDailyLogs = new collection.mutable.ListBuffer[(String, Long, Timestamp)]

      /*
      IMPORTANT! >_<

      Updates executed within a batch don't see the results of other updates within that batch.
      ie. 2 x "update count = count + 1;" will only increment count once.

      As long as we only have one update per s3key/stamp pair, this is fine. And as they are keys to the Map
      (at time of writing..) this is guaranteed (for small guarantees).
       */
      s.foreach {
        case ((s3Key, stamp), (ids, newCount)) =>
          count += 1
          if (count % 1000 == 0) {
            updateStatement.executeBatch()
            logger.info(count.toString)
          }
          val rs = findDaily(stamp, s3Key)
          if (rs.next()) {
            updateStatement.setLong(1, newCount)
            updateStatement.setInt(2, rs.getInt(1))
            updateStatement.addBatch()
          } else {
            newDailyLogs += ((s3Key, newCount, stamp))
          }
      }

      updateStatement.executeBatch()

      val accumulatedRecordIds = s.foldLeft(Nil: List[Int]) {(ids, v) => v._2._1 ::: ids}
      logger.info("Dropping accumulated records: %d" format accumulatedRecordIds.size)
      accumulatedRecordIds.grouped(5000).foreach(subset => {
        logger.info(".")
        Logs.where(_.id inSet subset).delete
      })
      logger.info("Adding new accumulations: %d" format newDailyLogs.size)
      newDailyLogs.grouped(5000).foreach(subset => {
        logger.info("*")
        (Logs.s3Key ~ Logs.scBytes ~ Logs.timestamp) insertAll (subset: _*)
      })
    }
    }
  }
  }

  def startDay[D](t: java.sql.Timestamp): java.sql.Timestamp =
    new java.sql.Timestamp(new org.joda.time.DateTime(t.getTime).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).toDate.getTime)
}
