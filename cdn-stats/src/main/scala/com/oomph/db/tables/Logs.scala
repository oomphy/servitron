package com.oomph.db.tables

import org.scalaquery.ql.extended.ExtendedTable

object Logs extends ExtendedTable[(Int, String, Long, java.sql.Timestamp)]("logs") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

  def s3Key = column[String]("s3_key", O.NotNull)

  def scBytes = column[Long]("sc_bytes", O.NotNull)

  def timestamp = column[java.sql.Timestamp]("timestamp", O.NotNull)

  def * = id ~ s3Key ~ scBytes ~ timestamp
}
