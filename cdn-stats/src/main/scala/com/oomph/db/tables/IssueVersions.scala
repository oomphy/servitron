package com.oomph.db.tables

import org.scalaquery.ql.extended.ExtendedTable

object IssueVersions extends ExtendedTable[(String, String, String)]("issue_versions") {
  def s3Key = column[String]("s3_key", O.NotNull)

  def applicationBundleId = column[String]("application_bundle_id", O.NotNull)

  def itunesProductId = column[String]("itunes_product_id", O.NotNull)

  def * = s3Key ~ applicationBundleId ~ itunesProductId
}
