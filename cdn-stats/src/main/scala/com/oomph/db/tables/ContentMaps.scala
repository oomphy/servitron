package com.oomph.db.tables

import org.scalaquery.ql.basic.BasicTable
import org.scalaquery.ql.{MappedTypeMapper, TypeMapper}
import argonaut.{DecodeJson, EncodeJson}
import java.sql.Blob
import java.io.ByteArrayOutputStream
import javax.sql.rowset.serial.SerialBlob
import java.nio.charset.Charset
import io.Source

case class GB(v: Float)
object GB {
  def fromBytes(v: Long) = GB(v.toFloat / 1024 / 1024)

  implicit val ordering: Ordering[GB] = Ordering.by(_.v)
  implicit val encode: EncodeJson[GB] = EncodeJson.FloatEncodeJson.contramap(_.v)
  implicit val decode: DecodeJson[GB] = DecodeJson.FloatDecodeJson.map(GB.apply _)
}

case class ContentMap(spaceUsage: Map[String, GB], hasSearchIndex: Boolean)

object ContentMap {
  implicit val encode: EncodeJson[ContentMap] =
    EncodeJson.jencode2L((ContentMap.unapply _).andThen(_.get))("space-usage", "has-search-index")
  implicit val decode: DecodeJson[ContentMap] =
    DecodeJson.jdecode2L(ContentMap.apply _)("space-usage", "has-search-index")
}

case class JsonColumn[T](value: T)

object JsonColumn {
  import argonaut._
  import Argonaut._

  implicit def jsonTypeMapper[T](implicit e: EncodeJson[T], d: DecodeJson[T]): TypeMapper[JsonColumn[T]] = MappedTypeMapper.base[JsonColumn[T], Blob]({ s =>
    val b = new ByteArrayOutputStream
    val asString = e.apply(s.value).toString()
    b.write(asString.getBytes(Charset.forName("UTF-8")))
    b.flush()
    new SerialBlob(b.toByteArray)
  }, { b =>
    val in = b.getBinaryStream
    val string = Source.fromInputStream(in, "UTF8").mkString("")
    JsonColumn(d(string.parseOption.getOrElse(sys.error("Unable to parse JSON in column")).hcursor).value.get)
  })
}

object ContentMaps extends BasicTable[(String, JsonColumn[ContentMap])]("content_maps") {
  def s3Key = column[String]("s3_key", O.NotNull, O.PrimaryKey)

  def contentBlob = column[JsonColumn[ContentMap]]("content_map", O.NotNull)

  def * = s3Key ~ contentBlob
}
