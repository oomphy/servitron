package com.oomph.db

import org.scalaquery.session.Session

object DB {
  type Action[A] = (Session => A)
}
