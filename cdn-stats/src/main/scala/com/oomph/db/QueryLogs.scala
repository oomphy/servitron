package com.oomph.db

import com.oomph.{S3ObjectDownload, DailyCount}
import org.scalaquery.ql.{Query, Join, SimpleFunction, Column}
import scala.collection.JavaConversions._
import tables.{Logs, IssueVersions}

object QueryLogs {
  import org.scalaquery.ql.extended.MySQLDriver.Implicit._

  def DATE(c: Column[java.sql.Timestamp]) = SimpleFunction[java.sql.Timestamp]("DATE").apply(Seq(c))

  def TIME(c: Column[java.sql.Timestamp]) = SimpleFunction[String]("TIME").apply(Seq(c))

  def downloadsForApplicationBundleId(application_bundle_id: String): DB.Action[java.util.List[DailyCount]] = { implicit s =>
    val r = for {
      Join(l, iv) <- Logs innerJoin IssueVersions on (_.s3Key === _.s3Key) if iv.applicationBundleId === application_bundle_id
      _ <- Query groupBy DATE(l.timestamp)
    } yield (DATE(l.timestamp) ~ l.scBytes.sum.get)
    r.list.map((DailyCount.apply _).tupled)
  }

  def downloadsForItunesProductId(itunes_product_id: String): DB.Action[java.util.List[DailyCount]] = { implicit s =>
    val r = for {
      Join(l, iv) <- Logs innerJoin IssueVersions on (_.s3Key === _.s3Key) if iv.itunesProductId === itunes_product_id
      d = DATE(l.timestamp)
      _ <- Query groupBy d
    } yield {(d ~ l.scBytes.sum.get)}
    r.list.map((DailyCount.apply _).tupled)
  }

  def insertLog(log: S3ObjectDownload): DB.Action[Unit] = { implicit s =>
    (Logs.s3Key ~ Logs.scBytes ~ Logs.timestamp).insert(log.s3_key, log.sc_bytes, new java.sql.Timestamp(log.timestamp.getTime))
  }
}
