package com.oomph

import clients.Dashboard
import db._
import dropwizard.{EnvironmentUtils, Tasks, RunnableTask, RailsDatabaseConfigurationFactory}

import com.oomph.resources._
import com.yammer.dropwizard.ScalaService

import com.yammer.dropwizard.db.DatabaseConfiguration
import com.yammer.dropwizard.config.Environment
import configuration.{Request, CdnStatsConfiguration}
import healthchecks.FtpHealthCheck
import cron.DashboardIngestor
import org.codehaus.jackson.map.util.ISO8601Utils
import java.util.Date
import com.yammer.dropwizard.logging.Log
import org.scalaquery.session.{Session, Database}
import java.util.concurrent._

class CdnStatsService extends ScalaService[CdnStatsConfiguration]("cdn-stats") {
  val log = Log.forClass(classOf[CdnStatsService])
  val defaultFlattenLimit = 100000

  def initialize(configuration: CdnStatsConfiguration, environment: Environment) {
    import configuration._
    val http = EnvironmentUtils.managedHttp(environment, httpClient)

    val slickDatabase: Database = initDB(configuration)
    val mmmm = new TransactionalInserter[S3ObjectDownload] {
      def withTransaction(f: ((S3ObjectDownload) => Unit) => Unit) {
        slickDatabase.withTransaction((session: Session) => f(download => QueryLogs.insertLog(download)(session)))
      }
    }

    environment.addHealthCheck(new FtpHealthCheck(credentials))

    val ingestor = new CdnIngestor(localLogDir, credentials, ftp.logDir, mmmm, cdnRoot)
    val dashboard = Dashboard(Request.fromConfiguration(configuration.dashboard))
    val dashboardIngestor = new DashboardIngestor(http, dashboard, slickDatabase)
    environment.addResource(new CdnStatsResource(slickDatabase))

    environment.addTask(RunnableTask("cdn_ingest", ingestor))
    environment.addTask(Tasks.make("dashboard_ingest", (m, out) => {
      val afterParam = m.get("created_after").asList().get(0)
      val afterDate: Date = ISO8601Utils.parse(afterParam)
      out.println("Ingesting after " + afterDate)
      dashboardIngestor.ingestAfter(afterDate)
      out.println("Done.")
    }))


    val logAggregator = new Runnable {def run() { slickDatabase.withSession(LogsAggregator.flatten(defaultFlattenLimit)((_: Session))) }}
    environment.addTask(RunnableTask("aggregate", logAggregator))

    val executor = environment.managedScheduledExecutorService("cdn-stats-%d", 2)
    ExecutorUtils.restartScheduleOnError(executor.scheduleWithFixedDelay(dashboardIngestor.recentlyR, 1, 30, TimeUnit.MINUTES))
    if (logAggregationEnabled) {
      ExecutorUtils.restartScheduleOnError(executor.scheduleWithFixedDelay(logAggregator, 1, 60, TimeUnit.MINUTES))
    }
    if (logIngestionEnabled) {
      ExecutorUtils.restartScheduleOnError(executor.scheduleWithFixedDelay(ingestor, 2, 60, TimeUnit.MINUTES))
    } else {
      log.info("CDN Ingestion is disabled.")
    }
  }

  def initDB(configuration: CdnStatsConfiguration): Database = {
    import configuration._
    val databaseConf: DatabaseConfiguration = RailsDatabaseConfigurationFactory(databaseYml, railsEnv, Some(database))
    Database.forURL(databaseConf.getUrl, user = databaseConf.getUser, password = databaseConf.getPassword)
  }
}
