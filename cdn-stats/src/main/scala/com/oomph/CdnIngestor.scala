package com.oomph

import com.yammer.dropwizard.logging.{Log => WizardLog}
import db.{DB, QueryLogs}
import java.io.File
import io.{Codec, Source}
import scalaz.{\/-, -\/, \/}
import org.scalaquery.session.{Database, Session}

object A {
  def downloadLogs(destDir: String, creds: Credentials, dir: String) {
    Ftp.enumerateFiles(creds, dir) { file =>
      val destFile = new File(new File(destDir), file.filename)
      file.download(destFile)
      file.delete
    }
  }
}

trait TransactionalInserter[T] {
  def withTransaction(f: (T => Unit) => Unit)
}

class CdnIngestor(destDir: String, creds: Credentials, dir: String, database: TransactionalInserter[S3ObjectDownload], origin: String) extends Runnable {
  val log = WizardLog.forClass(getClass)
  import A._
  import FileUtils._

  def run() {
    log.info("Grabbing logs from FTP")
    downloadLogs(destDir, creds, dir)
    log.info("Ingesting logs in %s" format destDir)
    filesIn(destDir).foreach { file =>
      database.withTransaction { (logSaver: S3ObjectDownload => Unit) =>
        log.info("* %s" format file)
        readLines(file).map(RawLog.parseLine _).foreach { rawLog =>
          insertOrFail(file, rawLog, logSaver)
        }
        file.delete
      }
    }
    log.info("Done.")
  }

  def readLines(f: File) = FileUtils.readGzipped(f).drop(1)

  def insertOrFail(file: File, l: String \/ RawLog, saver: S3ObjectDownload => Unit) {
    val dl = l.map(S3ObjectDownload.fromRaw(origin))
    dl match {
      case -\/(e) => {
        log.error("Failed to parse logs in %s: %s" format (file, e))
        throw new RuntimeException(e)
      }
      case \/-(x) => saver(x)
    }
  }
}

object FileUtils {
  def filesIn(dir: String): Array[File] = new File(dir).listFiles.filter(_.isFile)

  def readGzipped(f: File) = {
    val fin = new java.io.FileInputStream(f)
    val gfin = new java.util.zip.GZIPInputStream(fin)
    Source.fromInputStream(gfin)(Codec.UTF8).getLines
  }
}

object CdnIngestor {
  def readRawLogs(origin: String, f: File): String \/ List[S3ObjectDownload] = RawLog.parseAll(origin, FileUtils.readGzipped(f).drop(1).toList) // The first line is a header
}
