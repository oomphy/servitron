package com.oomph

import java.util.concurrent.{ExecutionException, Future}
import com.yammer.dropwizard.logging.Log

object ExecutorUtils {
  val log = Log.forThisClass()

  def restartScheduleOnError(addSchedule: => Future[_], handler: Option[Exception => Unit] = None) {
    val future = addSchedule
    val watchDog = new Runnable {
      def run() {
        while(true) try {
          future.get()
        } catch {
          case e: ExecutionException => {
            handler getOrElse ((v: Exception) => log.error(v, v.getMessage)) apply e
            restartScheduleOnError(addSchedule)
            return
          }
          case e: InterruptedException => return
        }
      }
    }
    new Thread(watchDog).start()
  }
}
