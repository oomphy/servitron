package com.oomph

import scala.util.parsing.combinator._
import scalaz.{-\/, \/-, Each, \/}
import scalaz.Scalaz._

// http://www.microsoft.com/technet/prodtechnol/WindowsServer2003/Library/IIS/676400bc-8969-4aa7-851a-9319490a9bbb.mspx?mfr=true
case class RawLog(
  timestamp: java.util.Date,
  time_taken: String,
  c_ip: String,
  filesize: String,
  s_ip: String,
  s_port: String,
  sc_status: String,
  sc_bytes: String,
  cs_method: String,
  cs_uri_stem: String,
  // --
  rs_duration: String,
  rs_bytes: String,
  c_referrer: String,
  /*c_user_agent: String,*/ // This is potentially very large and I don't think we care.
  customer_id: String,
  x_ec_custom_1: String
  )

object RawLog {
  type X[+A] = \/[String, A]

  def parseAll(origin: String, input: List[String]): String \/ List[S3ObjectDownload] = {
    val parseToDownload: String => String \/ S3ObjectDownload = (input: String) => {
      parseLine(input).map(S3ObjectDownload.fromRaw(origin) _).swap.map("%s: Error: %s" format (input, _)).swap
    }
    input.traverse[X, S3ObjectDownload](parseToDownload)
  }

  def parseTo[T](parser: String => String \/ T, source: Iterable[String], onError: String => Unit, onNext: T => Unit) {
    source.foreach { f =>
      parser(f) match {
        case -\/(l) => {
          onError(l)
         }
        case \/-(r) => onNext(r)
      }
    }
  }

  def parseLine(input: String): String \/ RawLog = parser.parse(parser.logLine, input) match {
    case parser.Success(ls, _) => \/.right(ls)
    case parser.Failure(msg, _) => \/.left(msg)
  }

  object parser extends JavaTokenParsers {
    val bit = stringLiteral | regex("[^\\s]+".r)
    val logLine = bit ~ bit ~ bit ~ bit ~ bit ~ bit ~ bit ~ bit ~ bit ~ bit ~ ("-" ~> bit) ~ bit ~ bit ~ bit ~ bit ~ bit ^^ {
      // The third last element is the ignored user agent
      case (a~b~c~d~e~f~g~h~i~j~k~l~m~_~o~p) => RawLog(new java.util.Date(a.toLong * 1000),b,c,d,e,f,g,h,i,j,k,l,m,o,p)
    }
  }
}
