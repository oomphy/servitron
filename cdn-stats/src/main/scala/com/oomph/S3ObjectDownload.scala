package com.oomph

import scalaz.\/
import org.codehaus.jackson.map.annotate.JsonSerialize
import org.codehaus.jackson.map.{SerializerProvider, JsonSerializer}
import org.codehaus.jackson.JsonGenerator
import java.text.SimpleDateFormat

case class S3ObjectDownload(timestamp: java.util.Date, s3_key: String, sc_bytes: Long) {
  lazy val day: java.util.Date = new org.joda.time.DateTime(timestamp).withTimeAtStartOfDay.toDate
}

case class IssueDownloadEvent(id: Int, when: java.util.Date, itunes_product_id: String, sc_bytes: Long)

@JsonSerialize(using=classOf[DailyCountSerializer])
case class DailyCount(day: java.util.Date, count: Long)

class DailyCountSerializer extends JsonSerializer[DailyCount] {
  val dateFormat = new SimpleDateFormat("yyyy-MM-dd")
  def serialize(value: DailyCount, jgen: JsonGenerator, provider: SerializerProvider) {
    jgen.writeStartObject()
    jgen.writeObjectField("day", dateFormat.format(value.day))
    jgen.writeObjectField("count", value.count)
    jgen.writeEndObject()
  }
}

object S3ObjectDownload {
  def fromRaw(origin: String)(rawLog: RawLog): S3ObjectDownload =
    S3ObjectDownload(rawLog.timestamp, extractS3Key(rawLog.cs_uri_stem, origin), bytes(rawLog.sc_bytes))

  def bytes(sc_bytes: String): Int = \/.fromTryCatch(sc_bytes.toInt).getOrElse(0)

  // Example hit
  // "http://oompf-cdn.mogeneration.com/805480/oompf.mogeneration.com/preview_images/preview_image_1194s/305/ipad/1.png"
  def extractS3Key(fullUrl: String, origin: String): String = fullUrl.split(origin + "/").last
}
