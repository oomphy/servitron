package com.oomph.resources

import com.yammer.metrics.annotation.Timed
import javax.ws.rs._
import javax.ws.rs.core.MediaType
import com.oomph.DailyCount

import java.util.Date
import com.yammer.dropwizard.logging.Log
import com.oomph.db.{DB, QueryLogs}
import java.util
import org.scalaquery.session.Database

@Path("/cdn-stats")
@Produces(Array(MediaType.APPLICATION_JSON))
class CdnStatsResource(db: Database) {
  type R[T] = Map[String, T]

  def point[A](a: A): R[A] = Map("result" -> a)

  val log = Log.forClass(getClass)

  @GET
  @Timed
  def downloads(@QueryParam("application_bundle_id") @DefaultValue("") application_bundle_id: String,
                @QueryParam("itunes_product_id") @DefaultValue("") itunes_product_id: String
                 ): R[java.util.List[DailyCount]] = {
    val counts: DB.Action[util.List[DailyCount]] = if (!application_bundle_id.isEmpty) {
      QueryLogs.downloadsForApplicationBundleId(application_bundle_id)
    } else if (!itunes_product_id.isEmpty) {
      QueryLogs.downloadsForItunesProductId(itunes_product_id)
    } else {
      sys.error("Expected one of s3_key, application_bundle_id or itunes_product_id")
    }
    point(db.withSession(counts))
  }

  def toMapByDay(xs: List[DailyCount]): Map[Date, Long] = xs.map(d => d.day -> d.count).toMap

  def merge(as: List[DailyCount], bs: List[DailyCount]): List[DailyCount] = {
    scalaz.std.map.unionWith(toMapByDay(as), toMapByDay(bs))(_ + _).map((DailyCount.apply _).tupled).toList
  }
}
