package com.oomph.dropwizard

import com.yammer.dropwizard.db.DatabaseConfiguration
import com.yammer.dropwizard.config.{Configuration, ConfigurationFactory}
import com.yammer.dropwizard.validation.Validator
import org.codehaus.jackson.annotate._
import org.hibernate.validator.constraints.NotEmpty
import java.io.File
import javax.validation.constraints.NotNull

@JsonIgnoreProperties(ignoreUnknown=true)
class RailsDatabaseConfiguration extends Configuration {
  @NotEmpty @JsonProperty
  var database: String = null
  @NotEmpty @JsonProperty
  val username: String = null
  @NotEmpty @JsonProperty
  val password: String = null
  @NotEmpty @JsonProperty
  val host: String = null

  def url = {
    val port = 3306
    "jdbc:mysql://" + host + ":" + port + "/" + database
  }

  def driverClass: String = {
    "com.mysql.jdbc.Driver"
  }

  def asDatabaseConfiguration: DatabaseConfiguration = {
    val conf = new DatabaseConfiguration

    conf.setDriverClass(driverClass)
    conf.setUser(username)
    conf.setPassword(Option(password).getOrElse(""))
    conf.setUrl(url)

    conf
  }
}

@JsonIgnoreProperties(ignoreUnknown=true)
class DatabaseYml extends Configuration {
  def configFor(s: String): RailsDatabaseConfiguration = s match {
    case "development" => development
    case "integration" => integration
    case _ => production
  }

  @JsonProperty
  val production = new RailsDatabaseConfiguration()

  @JsonProperty
  val development = new RailsDatabaseConfiguration()

  @JsonProperty
  val integration = new RailsDatabaseConfiguration()
}

object RailsDatabaseConfigurationFactory {
  def apply(yamlFile: String, environment: String, overrideDatabase: Option[String] = None): DatabaseConfiguration = {
    val databaseYml = readDatabaseYml(yamlFile)
    val railsConf = databaseYml.configFor(environment)
    overrideDatabase.foreach(v => railsConf.database = v)
    railsConf.asDatabaseConfiguration
  }

  def readDatabaseYml(yamlFile: String): DatabaseYml = {
    val temp = File.createTempFile("erbed-database", ".yml")
    temp.deleteOnExit()
    unErb(yamlFile, temp)
    ConfigurationFactory.forClass(classOf[DatabaseYml], new Validator).build(temp)
  }

  def unErb(file: String, dest: File) {
    import scala.sys.process._
    val erbCodes = """require 'erb'; puts ERB.new(IO.read("%s")).result(binding)""" format file
    Seq("ruby", "-e", erbCodes) #> dest !
  }
}

