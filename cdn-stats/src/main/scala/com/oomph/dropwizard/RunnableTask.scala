package com.oomph.dropwizard

import com.yammer.dropwizard.tasks.Task
import com.google.common.collect.ImmutableMultimap
import java.io.PrintWriter
import scala.Predef._

object RunnableTask {
  def apply(name: String, runnable: Runnable): Task = {
    new Task(name) {
      def execute(parameters: ImmutableMultimap[String, String], output: PrintWriter) {
        runnable.run()
      }
    }
  }
}

object Tasks {
  def make(name: String, f: (ImmutableMultimap[String, String], PrintWriter) => Unit): Task = {
    new Task(name) {
      def execute(parameters: ImmutableMultimap[String, String], output: PrintWriter) {
        f(parameters, output)
      }
    }
  }
}
