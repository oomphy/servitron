package com.oomph.dropwizard

import com.yammer.metrics.core.HealthCheck

object Health {
  def apply(name: String, f: => Unit) = new HealthCheck(name) {
    def check(): HealthCheck.Result = {
      f
      HealthCheck.Result.healthy()
    }
  }
}
