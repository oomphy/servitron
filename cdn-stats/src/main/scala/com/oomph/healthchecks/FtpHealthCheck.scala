package com.oomph.healthchecks

import com.oomph.{Ftp, Credentials}
import com.yammer.metrics.core.HealthCheck

class FtpHealthCheck(creds: Credentials) extends HealthCheck("ftp") {
  def check(): HealthCheck.Result = {
    Ftp.withFtp(creds) { _ =>
      HealthCheck.Result.healthy()
    }
  }
}
