

Maintaining the CDN Stats service
----

The log database is modified by 3 processes:

  * Ingestion - Logs are downloaded from the FTP, and each line in the log files becomes a record in the database
  * Aggregation - periodically, log lines are rolled up to the nearest hour - so we don't end up with a glut of records
  * Dashboard ingestion - all the issue_version records in dashboard are slurped into the database so that we can associate a url with an issue and publication

The first 2 of these are toggled by fields in the cdn-stats.yml config file.

If ingestion is happening successfully, then `/opt/cdn_stats/logs` will be empty (or only have a handful of recent files).

If there are a lot of logs here:

  * Check `/opt/cdn_stats/log.log` for errors.
  * Check for an error mentioning a specific log file - move that log file out of the `logs` directory (but save it somewhere)
  * Ingestion should now continue - you will need to watch for any errors on the rest of the files, as ingestion currently gets stuck on the first error
  * Fix the code, deploy, put the log file back in `logs`, and it'll eventually get picked up

If aggregation is happening successfully, then the Billables report in sachin will complete. When aggregation doesn't happen, there are too many records for that report to finish in a reasonable time.

When ingestion hasn't happened for a while, it can be useful to force aggregation:

  * Toggle `logAggregationEnabled` to false. Restart the service
  * $ curl -X POST http://localhost:8081/tasks/aggregate
  * Wait for that to finish. It will take a long time, maybe 20 mins. Repeat. many times
  * The "flatten_for_a_while.sh" script in ~deploy could help. Run it in a screen session. 
  * Note the /opt/cdn_stats/log.log will have information about what is being ingested or aggregated and any errors.

Restarting
---

To restart the service you need to:
  * sudo monit stop cdn_stats
  * ps aux | grep cdn
  * sudo kill -9 the process
  * sudo monit start cdn_stats
  * sudo monit restart cdn_stats DOES NOT WORK. IT LEAVES THE PROCESS RUNNING AND FAILS WHEN RESTARTING.

Other things
---

Tasks you can force the service to do are described on startup. For example

    POST    /tasks/gc (com.yammer.dropwizard.tasks.GarbageCollectionTask)
    POST    /tasks/cdn_ingest (null)
    POST    /tasks/dashboard_ingest (null)
    POST    /tasks/aggregate (null)

They need to be hit on the admin port, currently 8081


old notes
-----

database was initialized by hand:

  ssh deploy@database.server
  mysql -u root
  create database cdn_stats_production;
  grant all privileges on cdn_stats_production.* to 'deploy'@'%';
